package router

import (
	"net/http"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/handler"
)

func New(db db.DB) http.Handler {
	hr := httprouter.New()
	publicRouter(hr, db)
	recoveryPanicRouter(hr)
	healthcheck(hr)
	return hr
}

func publicRouter(r *httprouter.Router, db db.DB) {
	cache := cache.New()
	h := handler.NewRegistrationHandler(db, cache)
	r.POST("/register/consumer/", h.Register)
	r.GET("/job/:processID", h.RetrieveByProcessID)

	lh := handler.NewLegalProcessHandler(db, cache)
	r.GET("/court/process/:id", lh.GetByProcessID)
}

func recoveryPanicRouter(r *httprouter.Router) {
	r.PanicHandler = handler.PanicRecovery
}

func healthcheck(r *httprouter.Router) {
	r.GET("/healthcheck", handler.Healthcheck)
	r.GET("/healthcheck/queue", handler.HealthcheckQueue)
	r.GET("/healthcheck/cache", handler.HealthcheckCache)
	r.GET("/healthcheck/db", handler.HealthcheckDB)
}
