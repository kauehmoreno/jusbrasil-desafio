package core_test

import (
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/mocks"
)

type responseSuiteCase struct {
	suite.Suite
	url       string
	cachedUrl string
	server    *httptest.Server
}

func TestResponseSuiteCase(t *testing.T) {
	suite.Run(t, new(responseSuiteCase))
}

func (s *responseSuiteCase) SetupTest() {
	s.url = "http://localhost:9000"
	s.cachedUrl = fmt.Sprintf("%s/cached", s.url)

	s.server = mocks.Server(http.StatusOK, core.BaseResponse{
		Message: "working",
		Status:  http.StatusOK,
	}, time.Millisecond*50)
}

func (s *responseSuiteCase) TearDownTest() {
	s.server.Close()
}

func (s *responseSuiteCase) TestResponseWriteUnCached() {
	req, err := http.NewRequest(http.MethodGet, s.server.URL, nil)
	s.Require().NoError(err, "Err not expected on NewRequest")
	var cli http.Client

	resp, err := cli.Do(req)
	s.Require().NoError(err, "Should not return erro")
	defer resp.Body.Close()
	var response core.BaseResponse
	core.Decode(resp.Body, &response)
	s.Require().Equal("working", response.Message, "Message should be = working")
	s.Require().Equal(response.Status, http.StatusOK, "Status should be 200")
	s.Require().Equal(resp.Header.Get("Content-Type"), "application/json;charset=UTF-8", "Content-type does not match the expectation")
	s.Require().Equal(resp.Header.Get("Vary"), "Accept-Encoding", "Vary header key is different from expected")
	s.Require().Empty(resp.Header.Get("Cache-Control"), "Should not contains cache control on header response")
}

func (s *responseSuiteCase) TestCachedResponseWrite() {
	req, err := http.NewRequest(http.MethodGet, s.server.URL+"/cached", nil)
	s.Require().NoError(err, "Err not expected on NewRequest")
	var cli http.Client
	resp, err := cli.Do(req)
	s.Require().NoError(err, "Should not return erro")

	defer resp.Body.Close()
	var response core.BaseResponse
	core.Decode(resp.Body, &response)
	s.Require().Equal("working", response.Message, "Message should be = working")
	s.Require().Equal(response.Status, http.StatusOK, "Status should be 200")
	s.Require().Equal(resp.Header.Get("Content-Type"), "application/json;charset=UTF-8", "Content-type does not match the expectation")
	s.Require().Equal(resp.Header.Get("Vary"), "Accept-Encoding", "Vary header key is different from expected")
	s.Require().Equal(resp.Header.Get("Cache-Control"), "max-age=100", "Cache control is not equal as expected")
}
