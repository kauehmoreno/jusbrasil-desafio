package core_test

import (
	"fmt"
	"testing"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
)

func TestJobWatchBuild(t *testing.T) {
	processID := "10121132"
	jb := core.NewJobWatch(processID)

	if jb.JobWatch == "" {
		t.Errorf("job shouldnt be empty")
	}

	expected := fmt.Sprintf("http://localhost/job/%s", processID)

	if jb.JobWatch != expected {
		t.Errorf("value is not equal to expected one. current: %s - expecting:  %s", jb.JobWatch, expected)
	}
}
