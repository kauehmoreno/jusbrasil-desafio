package core

import (
	"bytes"
	"encoding/json"
	"net"
	"net/http"
	"time"
)

func netCli() *http.Client {
	netTrans := &http.Transport{
		Dial: (&net.Dialer{
			Timeout: 5 * time.Second,
		}).Dial,
		TLSHandshakeTimeout: 5 * time.Second,
	}

	cli := &http.Client{
		Timeout:   time.Second * 10,
		Transport: netTrans,
	}
	return cli
}

func Post(url string, header map[string]string, body interface{}) (*http.Response, error) {

	bBody, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(bBody))
	if err != nil {
		return nil, err
	}

	for key, value := range header {
		req.Header.Set(key, value)
	}

	net := netCli()
	return net.Do(req)
}

func Get(url string, header map[string]string) (*http.Response, error) {
	net := netCli()
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	for key, value := range header {
		req.SetBasicAuth(key, value)
	}

	return net.Do(req)
}
