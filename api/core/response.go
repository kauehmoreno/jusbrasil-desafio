package core

import (
	"net/http"
)

// BaseResponse is the api request response representation
type BaseResponse struct {
	Message string `json:"message"`
	Status  int    `json:"status"`
}

type header func(w http.ResponseWriter) http.ResponseWriter

func cached(maxAge string) header {
	return func(w http.ResponseWriter) http.ResponseWriter {
		w.Header().Set("Cache-Control", maxAge)
		return w
	}
}

func writeJSON(w http.ResponseWriter, data interface{}, status int, h ...header) {
	for _, param := range h {
		w = param(w)
	}

	w.Header().Set("Content-Type", "application/json;charset=UTF-8")
	w.Header().Set("Vary", "Accept-Encoding")
	w.WriteHeader(status)
	Encode(w, &data)
}

// WriteResponse returns data to response writer in json type
func WriteResponse(w http.ResponseWriter, data interface{}, status int) {
	writeJSON(w, data, status)
}

//WriterErrorResponse in case of error allow cli to response with a msg and error
func WriterErrorResponse(w http.ResponseWriter, msg string, status int) {
	writeJSON(w, BaseResponse{
		Message: msg,
		Status:  status,
	}, status)
}

// WriteCacheResponse include max-age cache on response writer
func WriteCacheResponse(w http.ResponseWriter, data interface{}, statusCode int, maxAge string) {
	writeJSON(w, data, statusCode, cached(maxAge))
}
