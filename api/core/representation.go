package core

import (
	"strings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"
)

//RegisterJob payload from registration
type RegisterJob struct {
	ProcessID string `json:"process_id"`
	Court     string `json:"court"`
}

//JobResponse response if path of job
type JobResponse struct {
	JobWatch string `json:"job_watch"`
}

//NewJobWatch build struct based on processID
func NewJobWatch(processID string) JobResponse {
	var s strings.Builder
	s.WriteString(settings.Get().Host)
	s.WriteString("/job/")
	s.WriteString(processID)
	path := s.String()
	return JobResponse{
		JobWatch: path,
	}
}
