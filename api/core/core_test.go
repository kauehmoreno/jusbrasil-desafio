package core_test

import (
	"bytes"
	"encoding/json"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
)

type coreSuiteCase struct {
	suite.Suite
}

func TestCoreCase(t *testing.T) {
	suite.Run(t, new(coreSuiteCase))
}

func (s coreSuiteCase) TestDecodeDataBasedOnDataType() {
	t := struct {
		Name string
	}{"Teste"}
	data, err := json.Marshal(t)
	s.Require().NoError(err)

	readerIP := bytes.NewBuffer(data)
	var expected struct{ Name string }
	core.Decode(readerIP, &expected)
	s.Require().Equal(expected.Name, t.Name, "Should decode to expected value type correctly")
}

func (s coreSuiteCase) TestDecodeDataWithErrorOnWrongGivenType() {
	t := struct {
		Name string
	}{"Teste"}
	data, err := json.Marshal(t)
	s.Require().NoError(err)

	readerIP := bytes.NewBuffer(data)

	var expected string
	erro := core.Decode(readerIP, &expected)
	s.Require().Error(erro)
}

func (s coreSuiteCase) TestEncodeToWriteGivenType() {
	b := bytes.NewBuffer([]byte{})
	t := struct {
		Key   string
		Value string
	}{"KEY", "VALUE"}
	err := core.Encode(b, t)
	s.Require().NoError(err)
	s.Require().Condition(func() bool {
		return b.Len() > 0
	}, "Should have data writed in it")
}

func (s coreSuiteCase) TestGenerateUUID() {
	uuid := core.GenerateUUID()
	s.Require().NotEmpty(uuid, "uuid should not be empty ")
	s.Require().Equal(36, len(uuid), "Should contains 36 char")
}

func (s coreSuiteCase) TestGenerateTimeStamp() {
	date := core.GetCurrTimeStamp()
	s.Require().NotEmpty(date, "date should contain string")
	s.Require().Condition(func() bool {
		return len(date) == 19
	})
}

func (s coreSuiteCase) TestFormatDate() {
	t := time.Date(2019, 12, 29, 14, 15, 29, 29, time.Local)
	date := core.FormatDate("2006-01-02", t)
	s.Require().Equal("2019-12-29", string(date), "both date should be equal - but they are not")
}
