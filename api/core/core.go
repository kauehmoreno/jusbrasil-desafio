package core

import (
	cryprand "crypto/rand"
	"encoding/json"
	"fmt"
	"io"
	"time"
)

// Decode allows customization of decoder read it json protocol
// but it could change to msgpack without sideaffect on client implementation
func Decode(body io.Reader, expected interface{}) error {
	return json.NewDecoder(body).Decode(&expected)
}

// Encode encode data based on writer implementation
// it allow changes on encoder protocol without sideaffect on client implementation
// its json based but it could be msgpack
func Encode(w io.Writer, data interface{}) error {
	return json.NewEncoder(w).Encode(&data)
}

// GenerateUUID gera uuid
func GenerateUUID() string {
	uuid := make([]byte, 16)
	n, err := io.ReadFull(cryprand.Reader, uuid)
	if n != len(uuid) || err != nil {
		return ""
	}
	uuid[8] = uuid[8]&^0xc0 | 0x80
	uuid[6] = uuid[6]&^0xf0 | 0x40
	return fmt.Sprintf("%x-%x-%x-%x-%x", uuid[0:4], uuid[4:6], uuid[6:8], uuid[8:10], uuid[10:])
}

// FormatDate implements a more Efficiant way of format a string .. It used internaly by time.Time to convert any time
// method format wraps appendFormat internally.
// AppendeFormat helps memoray allocation and use stack placament instead of heap which allocate in runtime
// Stack allocation requires that memory footprint can be determined at compile time.
// Otherwise a dynamic allocation onto the heap occurs at runtime.
//  malloc must search for a chunk of free memory large enough to hold the new value
func FormatDate(format string, t time.Time) []byte {
	f := []byte("")
	return t.AppendFormat(f, format)
}

// GetCurrTimeStamp Pega o tempo atual considerando min e segundo
func GetCurrTimeStamp() string {
	text := FormatDate("2006-01-02 15:04:05", time.Now())
	return string(text)
}
