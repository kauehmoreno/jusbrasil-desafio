package consumer

import (
	"errors"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/cenkalti/backoff"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"

	nats "github.com/nats-io/go-nats"
)

// Consumer interface ...
type Consumer interface {
	Subscribe(topicName, groupName string, fn func([]byte) error) error
	Error() error
}

type consumerBase struct {
	ticker    time.Duration
	retryTime time.Duration
	err       error
	con       *nats.Conn
}

// Options allow set a bunch of options to consumer
type Options func(*consumerBase)

// WithTicker allow to customize time of repeatly execution - default is one minute
func WithTicker(t time.Duration) Options {
	return func(c *consumerBase) {
		c.ticker = t
	}
}

// RetryTime allow to customize the total number of retry whenever a backoff
// operation start - default is two minutes
func RetryTime(t time.Duration) Options {
	return func(c *consumerBase) {
		c.retryTime = t
	}
}

func getInstance() consumerBase {
	con, err := queue.Connect()
	if err != nil {
		return consumerBase{err: err}
	}
	c := consumerBase{
		ticker:    time.Minute,
		con:       con,
		retryTime: time.Minute * 2,
		err:       nil,
	}
	return c
}

// New based on options it will return a Consumer interface allowing
// client to only subscribe into a channel e execute a certain function
// if not customized by options consumer will use default value of one minute repeatly
// execution and max backoff time of two minutes before fail and push it back into queue
func New(opts ...Options) Consumer {
	c := getInstance()
	for _, opt := range opts {
		opt(&c)
	}
	return c
}

// Subscribe allow a consumer to subscribe into a topic specifing group name
// it contains backoff operation in case of failuring on execute function passed as params
// after all retry it will put it back it queue to be consumed again
func (c consumerBase) Subscribe(topic, groupName string, fn func(msg []byte) error) error {
	if c.Error() != nil {
		return c.Error()
	}
	if !c.con.IsConnected() {
		return errors.New("[consumer] erro on subscribe provider is not connected")
	}
	_, err := c.con.QueueSubscribe(topic, groupName, func(msg *nats.Msg) {
		if err := fn(msg.Data); err != nil {
			log.Warningf("[consumer] backoff initialized... topic:%s group:%s", topic, groupName)
			go func() {
				if err := c.retry(msg.Data, fn); err != nil {
					logger := log.Fields{}
					logger["topic"] = topic
					logger["groupName"] = groupName
					logger["err"] = err
					log.WithFields(logger).Error("[consumer] could not excecute function base on the given message - queue it back")
					// TODO revise it here
					data, err := queue.ErrorMsg(string(msg.Data), err)
					if err != nil {
						log.WithFields(logger).Error("[consumer] could not convert error msg to byte - fatal fail - message will be lost")
						return
					}
					errTopic := queue.TopicWithErr(topic)
					c.con.Publish(errTopic, data)
				}
			}()
		}
	})
	if err != nil {
		return err
	}

	ticker := time.NewTicker(c.ticker)
	defer ticker.Stop()
	defer c.con.Close()
	for range ticker.C {
	}
	return nil
}

func (c consumerBase) Error() error {
	return c.err
}

// retry is a backoff operation to be used it will take retryTime of 2 minute
// if it is not override a new one on consumer Options
func (c consumerBase) retry(data []byte, fn func(data []byte) error) error {
	retry := backoff.NewExponentialBackOff()
	retry.MaxElapsedTime = c.retryTime
	operation := func() error {
		return fn(data)
	}
	return backoff.Retry(operation, retry)
}
