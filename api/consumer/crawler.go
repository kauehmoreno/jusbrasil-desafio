package consumer

import (
	"context"
	"errors"
	"math/rand"
	"time"

	"github.com/gocolly/colly"
)

var (
	//ErrCaptchaIdentified whenever a page implements captcha it will return this error
	ErrCaptchaIdentified = errors.New("erro to scrap page contains recaptcha")

	// ErrNoResult whenever there is not result to a specfic process on each stage
	ErrNoResult = errors.New("process does not have any data on this stage")
)

// Crawler defining behavior of each composible crawler type
type Crawler interface {
	Start() error
	Callback([]byte) error
}

type Scrambler struct {
	URLs          []string
	numOfCrawlers int
	collector     *colly.Collector
	Consumer
	maxConcurrent int
	delay         time.Duration
}

// Rules define rules to be set on crawler type
type Rules func(*Scrambler)

//RateLimit based on delay and concurrent number it will strict crawler behavior to avoid shutdown services consuptions
func RateLimit(concurrent int, delay time.Duration) Rules {
	return func(c *Scrambler) {
		c.collector.Limit(&colly.LimitRule{
			DomainGlob:  "*",
			Parallelism: concurrent,
			Delay:       delay,
		})
		c.maxConcurrent = concurrent
		c.delay = delay
	}
}

// NumOfCrawlers for a set o urls - this is going to be used as number of consumers each crawler will have
// this number may optimize execution throughput
func NumOfCrawlers(num int) Rules {
	return func(c *Scrambler) {
		c.numOfCrawlers = num
	}
}

// URLVisitors a crawler instance may have more than one url to visit
func URLVisitors(urls ...string) Rules {
	return func(c *Scrambler) {
		c.URLs = append(c.URLs, urls...)
	}
}

func NewCrawler(rules ...Rules) *Scrambler {
	collector := colly.NewCollector(
		colly.Async(true),
		colly.MaxDepth(2),
	)

	collector.Limit(&colly.LimitRule{
		DomainGlob:  "*",
		Parallelism: 1,
		Delay:       time.Second,
	})

	crl := &Scrambler{
		URLs:          []string{},
		numOfCrawlers: 1,
		collector:     collector,
		maxConcurrent: 1,
		delay:         time.Second,
	}

	for _, rule := range rules {
		rule(crl)
	}
	return crl
}

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

//RandomString use to randomize user agent
func RandomString() string {
	b := make([]byte, rand.Intn(10)+10)
	for i := range b {
		b[i] = letterBytes[rand.Intn(len(letterBytes))]
	}
	return string(b)
}

//OnHTMLAction will execute all queries and fns in the same order as passed ...
// query[0] fns[0] - query[1] fns[1] and so on
func (s *Scrambler) OnHTMLAction(ctx context.Context, url string, queries []string, fns []func(*colly.HTMLElement)) error {
	ch := make(chan struct {
		err  error
		done bool
	}, 2)
	collector := s.collector.Clone()

	collector.OnRequest(func(r *colly.Request) {
		r.Headers.Set("User-Agent", RandomString())
	})

	collector.OnError(func(resp *colly.Response, err error) {
		ch <- struct {
			err  error
			done bool
		}{err, false}
		return
	})
	for i, query := range queries {
		collector.OnHTML(query, fns[i])
	}

	collector.OnScraped(func(resp *colly.Response) {
		ch <- struct {
			err  error
			done bool
		}{nil, true}
		return
	})

	collector.Visit(url)
	collector.Wait()

	select {
	case done := <-ch:
		return done.err
	case <-ctx.Done():
		return ctx.Err()
	}
}
