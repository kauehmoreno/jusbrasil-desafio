package consumer

import (
	"context"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type testTJMSSuiteCase struct {
	suite.Suite
	processID, firstStageURL, expectedFirstStageURL, secondStageURL, expectedSecondStageURL string
}

func TestTJMSCase(t *testing.T) {
	suite.Run(t, new(testTJMSSuiteCase))
}

func (suite *testTJMSSuiteCase) SetupSuite() {
	suite.processID = "0821901-51.2018.8.12.0001"
	suite.firstStageURL = "https://esaj.tjms.jus.br/cpopg5/search.do"
	suite.expectedFirstStageURL = "https://esaj.tjms.jus.br/cpopg5/search.do?conversationId=&dadosConsulta.localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&dadosConsulta.tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=0821901-51.2018&foroNumeroUnificado=0001&dadosConsulta.valorConsultaNuUnificado=0821901-51.2018.8.12.0001&dadosConsulta.valorConsulta=&uuidCaptcha="
	suite.secondStageURL = "https://esaj.tjms.jus.br/cposg5/search.do"
	suite.expectedSecondStageURL = "https://esaj.tjms.jus.br/cposg5/search.do?conversationId=&paginaConsulta=1&localPesquisa.cdLocal=-1&cbPesquisa=NUMPROC&tipoNuProcesso=UNIFICADO&numeroDigitoAnoUnificado=0821901-51.2018&foroNumeroUnificado=0001&dePesquisaNuUnificado=0821901-51.2018.8.12.0001&dePesquisa=&uuidCaptcha="

}

func (suite testTJMSSuiteCase) TestbuildAccessingURLFromFirstStage() {
	cons := NewTJMS(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.firstStageURL),
	)

	url, err := cons.buildAcessingURL(suite.firstStageURL, suite.processID)
	suite.Require().NoError(err, "should not fail on build acccessing url")
	expectation := strings.Split(suite.expectedFirstStageURL, "?")
	expectation = strings.Split(expectation[1], "&")
	for _, expected := range expectation {
		suite.Require().Contains(url, expected, fmt.Sprintf("url should contains %s", expected))
	}
}

func (suite testTJMSSuiteCase) TestbuildAccessingURLFromSecondStage() {
	cons := NewTJMS(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.firstStageURL, suite.secondStageURL),
	)

	url, err := cons.buildAcessingURL(suite.secondStageURL, suite.processID)
	suite.Require().NoError(err, "should not fail on build acccessing url")
	expectation := strings.Split(suite.expectedSecondStageURL, "?")
	expectation = strings.Split(expectation[1], "&")
	for _, expected := range expectation {
		suite.Require().Contains(url, expected, fmt.Sprintf("url should contains %s", expected))
	}
}

func (suite testTJMSSuiteCase) TestScrapFirstStageShouldFailBasedOnContextTimeoutOnceSavingOnDBWillTakeLonger() {
	cons := NewTJMS(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.firstStageURL, suite.secondStageURL),
	)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	err := cons.FirstStage(ctx, suite.processID)
	suite.Require().Error(err)
	suite.Require().EqualError(err, context.DeadlineExceeded.Error())
}
