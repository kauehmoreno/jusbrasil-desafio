package consumer

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/court"
)

func normalizePrice(value string) (float64, error) {
	actionPrice := strings.Split(value, " ")
	price := actionPrice[len(actionPrice)-1]
	price = strings.Replace(price, ".", "", -1)
	price = strings.Replace(price, ",", ".", -1)
	return strconv.ParseFloat(price, 64)
}

func normalizedistributionDate(date string) (time.Time, error) {
	dateInfo := strings.Split(date, " ")
	cDate := make([]string, 1)
	cDate = append(cDate, dateInfo[0], fmt.Sprintf("%s:00", dateInfo[2]))
	data := strings.Trim(strings.Join(cDate, " "), " ")
	return time.Parse("02/01/2006 15:04:05", data)
}

func extractProcessMembers(repr [][]string) (court.Answerable, error) {
	var answerable court.Answerable
	for _, rep := range repr {
		var representative court.Representative
		lastEl := rep[len(rep)-1]
		for i, member := range rep {
			member = strings.TrimSpace(member)
			if member == lastEl || member == "" {
				continue
			}

			if patternMatchingRepresenter(member) {
				if patternMatchingReprNameOnNextLine(member) {
					representative.Representer = rep[i+1]
					continue
				}
				representative.Representer = member
				continue
			}

			var lawyer court.Lawyer
			gender := patternMatchingGenderLawyer(member)
			if gender != "" {
				lawyer.Gender = gender
				if patternMatchingLaywerNameOnNextLine(member) {
					lawyer.Name = rep[i+1]
					representative.Lawyers = append(representative.Lawyers, lawyer)
					continue
				}
				lawyer.Name = member
				representative.Lawyers = append(representative.Lawyers, lawyer)
			}

			if member != lawyer.Name {
				if i == 0 {
					representative.Name = member
					continue
				}
				if !patternMatchingPreviusIsReprOrLawyer(rep[i-1]) {
					representative.Name = member
				}
			}

		}
		if patternMatchingDefendant(lastEl) {
			answerable.Defendant = append(answerable.Defendant, representative)
			continue
		}
		answerable.Accuser = append(answerable.Accuser, representative)
	}
	return answerable, nil
}

func patternMatchingRepresenter(data string) bool {
	data = strings.ToLower(data)
	switch {
	case strings.Contains(data, "reprtate:") || strings.Contains(data, "repreleg:"):
		return true
	default:
		return false
	}
}

func patternMatchingPreviusIsReprOrLawyer(data string) bool {
	data = strings.ToLower(data)
	switch {
	case patternMatchingRepresenter(data):
		return true
	case strings.Contains(data, "advogada") || strings.Contains(data, "advogado"):
		return true
	default:
		return false
	}
}

func patternMatchingGenderLawyer(data string) string {
	data = strings.ToLower(data)
	switch {
	case strings.Contains(data, "advogada"):
		return "F"
	case strings.Contains(data, "advogado"):
		return "M"
	default:
		return ""
	}
}

func patternMatchingLaywerNameOnNextLine(data string) bool {
	data = strings.ToLower(data)
	switch {
	case data == "advogada:" || data == "advogado:":
		return true
	default:
		return false
	}
}

func patternMatchingReprNameOnNextLine(data string) bool {
	data = strings.ToLower(data)
	switch {
	case data == "reprtate:" || data == "repreleg:":
		return true
	default:
		return false
	}
}

func patternMatchingDefendant(data string) bool {
	data = strings.ToLower(data)
	switch {
	case strings.Contains(data, "agravado") || strings.Contains(data, "reqdo") || strings.Contains(data, "réu"):
		return true
	default:
		return false
	}
}

func patternMatchingAccuser(data string) bool {
	data = strings.ToLower(data)
	switch {
	case data == "agravante:" || data == "reqte:" || data == "autora:":
		return true
	default:
		return false
	}
}

func extractForumNum(coding string) string {
	nums := strings.Split(coding, ".")
	return nums[len(nums)-1][1:]
}

func extractUnifiedDigit(coding string) string {
	infos := strings.Split(coding, ".")
	info := make([]string, 2)
	copy(info, infos[:2])
	return strings.Join(info, ".")
}
