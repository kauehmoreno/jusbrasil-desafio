package consumer_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/nats-io/nats-server/test"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/consumer"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"

	"github.com/stretchr/testify/suite"
)

type testConsumerBaseSuiteCase struct {
	suite.Suite
	topic, group, msg string
}

func TestConsumerBaseCase(t *testing.T) {
	suite.Run(t, new(testConsumerBaseSuiteCase))
}

func (suite *testConsumerBaseSuiteCase) SetupSuite() {
	suite.topic = "topic-test"
	suite.group = "test"
	suite.msg = "hello test msg"
}

func (suite testConsumerBaseSuiteCase) TestInstanceOfConsumerBaseShouldHaveErrOnNew() {
	c := consumer.New()
	suite.Require().Error(c.Error(), "should return error once provider is not up")
}

func (suite testConsumerBaseSuiteCase) TestSubscriptionFail() {
	c := consumer.New()
	err := c.Subscribe(suite.topic, suite.group, func(data []byte) error {
		return nil
	})
	suite.Require().Error(err, "should fail on subscribe into a not connected queue")
}

func (suite testConsumerBaseSuiteCase) TestInstanceSuccessfullyConnected() {
	s := test.RunDefaultServer()
	s.Start()
	c := consumer.New()
	s.Shutdown()
	suite.Require().NoError(c.Error(), "should contains error once provider is not available")
}

func (suite testConsumerBaseSuiteCase) TestSubscribeExecution() {
	s := test.RunDefaultServer()
	s.Start()
	var c consumer.Consumer
	c = consumer.New(consumer.WithTicker(time.Second))

	ch := make(chan []byte, 10)
	go func(ch chan<- []byte) {
		if c.Error() != nil {
			c = consumer.New(consumer.WithTicker(time.Second))
		}
		err := c.Subscribe(suite.topic, suite.group, func(data []byte) error {
			ch <- data
			return nil
		})
		t := suite
		t.NoError(err, "should not fail to subscribe into topic")
	}(ch)

	queue, err := queue.Connect()
	suite.Require().NoError(err, "should not fail to connect into provider")

	for i := 0; i <= 5; i++ {
		err = queue.Publish(suite.topic, []byte(suite.msg))
		suite.Require().NoError(err, "should not fail to publish msg into queue")
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	select {
	case msg := <-ch:
		s.Shutdown()
		suite.Require().Equal(string(msg), suite.msg, "both msg should match")
	case <-ctx.Done():
		s.Shutdown()
		suite.FailNow("fail due timeout operation - subscribe didnt delivery msgs")
	}
}

func (suite testConsumerBaseSuiteCase) TestBackOffInitializationInCaseOfError() {
	s := test.RunDefaultServer()
	s.Start()
	defer s.Shutdown()
	c := consumer.New(consumer.WithTicker(time.Second), consumer.RetryTime(time.Second*5))
	ch := make(chan error, 1)
	go func(ch chan<- error) {
		c.Subscribe(suite.topic, suite.group, func(data []byte) error {
			err := errors.New("fake error")
			ch <- err
			return err
		})
	}(ch)

	queue, err := queue.Connect()
	suite.Require().NoError(err, "should not fai to connect into queue")
	err = queue.Publish(suite.topic, []byte(suite.msg))
	suite.Require().NoError(err, "should not fai to publish msg on queue")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	select {
	case err := <-ch:
		suite.Require().Error(err, "should fail after backoff operation")
	case <-ctx.Done():
		suite.FailNow("fail due timeout operation - subscribe didnt delivery msgs")
	}
}
