package consumer

import (
	"context"
	"net/url"
	"strings"
	"sync"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"github.com/gocolly/colly"
	"github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/court"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/job"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"
)

type TJMS struct {
	*Scrambler
	topicName, groupName, host string
	err                        chan struct{ err error }
	legalProcess               court.LegalProcess
}

func NewTJMS(cm Consumer, rule ...Rules) TJMS {
	crawler := NewCrawler(rule...)
	crawler.Consumer = cm
	return TJMS{
		crawler,
		queue.TopicLegalProcessTJMS,
		"TJMS",
		"https://esaj.tjms.jus.br",
		make(chan struct{ err error }, 10),
		court.LegalProcess{},
	}
}

func (t *TJMS) Start() error {
	ch := make(chan struct{ err error }, t.numOfCrawlers)

	var wg sync.WaitGroup

	wg.Add(t.numOfCrawlers)
	logrus.Warnf("starting %d workers from tjms...", t.numOfCrawlers)
	for i := 1; i <= t.numOfCrawlers; i++ {
		go func() {
			err := t.Subscribe(t.topicName, t.groupName, t.Callback)
			ch <- struct{ err error }{err}
			wg.Done()
		}()
	}
	wg.Wait()
	close(ch)

	for subscription := range ch {
		if subscription.err != nil {
			return subscription.err
		}
	}

	return nil
}

// Callback will be invoked every time a msg arrives
func (t TJMS) Callback(data []byte) error {

	processID := string(data)
	var err error
	ctx, cancel := context.WithTimeout(context.Background(), time.Minute*10)
	defer cancel()

	if err = t.FirstStage(ctx, processID); err != nil {
		logrus.Errorf("[TJMS FirstStage] error on scrap processID:%s - %v ", processID, err)
		if err = t.SecondStage(ctx, processID); err != nil {
			logrus.Errorf("[TJMS SecondStage] error on scrap processID:%s - %v ", processID, err)
		}
	}
	return err
}

//FirstStage is the representation of crawling a legal process on first stage
func (t *TJMS) FirstStage(ctx context.Context, coding string) error {
	queries := []string{
		// "#uuidCaptcha",
		"#recaptcha-token",
		"body > div > table:nth-child(4) > tbody > tr > td > div:nth-child(7) > table.secaoFormBody > tbody",
		"#tableTodasPartes",
		"#tabelaTodasMovimentacoes",
		"#mensagemRetorno",
	}
	fns := []func(*colly.HTMLElement){
		// t.WithCaptcha,
		t.WithCaptcha,
		t.ExtractMainInfo,
		t.ExtractAnswarable,
		t.ExtractAllMoving,
		t.NotFound,
	}

	url, err := t.buildAcessingURL(t.URLs[0], coding)
	if err != nil {
		return err
	}

	if err := t.OnHTMLAction(ctx, url, queries, fns); err != nil {
		return err
	}

	select {
	case err := <-t.err:
		if err.err != nil {
			return t.RegisterResult(coding, 1, err.err)
		}
		return t.RegisterResult(coding, 1, nil)
	case <-ctx.Done():
		return ctx.Err()
	}
}

//SecondStage crawling a legal process on second stage of a specific court
func (t *TJMS) SecondStage(ctx context.Context, coding string) error {
	queries := []string{
		"#uuidCaptcha",
		"#recaptcha-token",
		"#listagemDeProcessos",
		"body > div > table:nth-child(4) > tbody > tr > td > div:nth-child(7) > table.secaoFormBody > tbody",
		"#tableTodasPartes",
		"#tabelaTodasMovimentacoes",
		"#mensagemRetorno",
	}
	fns := []func(*colly.HTMLElement){
		t.WithCaptcha,
		t.WithCaptcha,
		t.VisitLink,
		t.ExtractMainInfo,
		t.ExtractAnswarable,
		t.ExtractAllMoving,
		t.NotFound,
	}

	url, err := t.buildAcessingURL(t.URLs[1], coding)
	if err != nil {
		return err
	}

	if err := t.OnHTMLAction(ctx, url, queries, fns); err != nil {
		return err
	}

	select {
	case err := <-t.err:
		if err.err != nil {
			logrus.Errorf("[consumer.crawler.registerResult.tjms] erro on crawler %v", err.err)
			return t.RegisterResult(coding, 2, err.err)
		}
		return t.RegisterResult(coding, 2, nil)
	case <-ctx.Done():
		return ctx.Err()
	}
}

//RegisterResult if there is an error it will
func (t *TJMS) RegisterResult(processID string, stage int, err error) error {
	database, erro := db.Connect()
	if erro != nil {
		return erro
	}
	storage := cache.New()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	ch := make(chan struct{ err error })

	if err != nil {
		go func() {
			err := job.Cancel(ctx, processID, err)(storage, database)
			ch <- struct{ err error }{err}
			return
		}()

		select {
		case <-ctx.Done():
			logrus.Errorf("[consumer.crawler.registerResult.tjms] context timeout to register on db %v", ctx.Err())
			return err
		case result := <-ch:
			return result.err
		}
	}

	lp := court.NewLegalProcess("tjms")
	lp.LegalProcess = t.legalProcess
	lp.LegalProcess.Stage = stage

	manager, err := court.NewManager(court.Database(database), court.Listeable(storage), court.Cacheable(storage))
	if err != nil {
		return err
	}

	go func() {
		err := job.Done(ctx, lp.LegalProcess.ProcessID)(storage, database)
		ch <- struct{ err error }{err}
		return
	}()
	select {
	case <-ctx.Done():
		logrus.Errorf("[consumer.crawler.registerResult.tjms] context timeout to register on db %v", ctx.Err())
		return ctx.Err()
	case result := <-ch:
		if result.err != nil {
			logrus.Errorf("[consumer.crawler.registerResult.tjms] error on finish job %v", err)
		}
		return manager.Save(ctx, lp)
	}
}

// VisitLink goes throw link to visit and scrap next page
func (t TJMS) VisitLink(e *colly.HTMLElement) {
	e.ForEach("a", func(_ int, e *colly.HTMLElement) {
		t.collector.Visit(t.host + e.Attr("href"))
	})
}

// ExtractMainInfo will extract main info form a legal process
func (t *TJMS) ExtractMainInfo(e *colly.HTMLElement) {
	result := make(map[string]string)
	e.ForEach("tr", func(i int, el *colly.HTMLElement) {
		var text string
		label := el.ChildText("td:nth-child(1) > label")
		if label == "" {
			label = "setor"
		}
		text = el.ChildText("td:nth-child(2) > table > tbody > tr > td > span:nth-child(1)")
		if text == "" {
			text = el.ChildText("td:nth-child(2) > span")
		}
		result[label] = text
	})

	price, err := normalizePrice(result["Valor da ação:"])
	if err != nil {
		logrus.Errorf("[TJMS] erro on normalizePrice %v", err)
		t.err <- struct{ err error }{err}
		return
	}
	date, err := normalizedistributionDate(result["Distribuição:"])
	if err != nil {
		logrus.Errorf("[TJMS] erro on normalizedistributionDate %v", err)
		t.err <- struct{ err error }{err}
		return
	}

	dateInfo := strings.Split(result["Distribuição:"], " ")
	t.legalProcess = court.LegalProcess{
		LegalActionPrice: price,
		Classe:           result["Classe:"],
		Judge:            result["Juiz:"],
		Issue:            result["Assunto:"],
		ProcessID:        result["Processo:"],
		Area:             result["setor"],
		ID:               core.GenerateUUID(),
		Distribution: court.Distribution{
			Date: date,
			Name: dateInfo[len(dateInfo)-1],
		},
	}
	t.err <- struct{ err error }{nil}
}

//ExtractAnswarable will extract all answarable present on legal process
func (t *TJMS) ExtractAnswarable(e *colly.HTMLElement) {
	rs := [][]string{}
	e.ForEach("tr", func(_ int, el *colly.HTMLElement) {
		id := el.ChildText("td:nth-child(1)")
		members := strings.Split(el.ChildText("td:nth-child(2)"), "\n")
		members = append(members, id)
		rs = append(rs, members)
	})

	anwserable, err := extractProcessMembers(rs)
	if err != nil {
		logrus.Errorf("[TJMS] could not extrat process member from legal process %v", err)
		t.err <- struct{ err error }{err}
		return
	}
	t.legalProcess.Answerable = anwserable
	t.err <- struct{ err error }{nil}
}

// ExtractAllMoving all moving
func (t *TJMS) ExtractAllMoving(e *colly.HTMLElement) {
	var legalStage court.LegalProcesStages
	e.ForEach("tr", func(_ int, el *colly.HTMLElement) {
		data := el.ChildText("td:nth-child(1)")
		date, err := time.Parse("02/01/2006", data)
		if err != nil {
			logrus.Errorf("[TJMS] fail to convert stage date of legal process %v", err)
			t.err <- struct{ err error }{err}
			return
		}
		txt := el.ChildText("td:nth-child(3)")
		txt = strings.Replace(txt, "\n", " ", -1)
		legalStage.Stages = append(legalStage.Stages, court.Stage{
			Date:   date,
			Action: txt,
		})
	})

	t.legalProcess.Stages = legalStage.Stages
	t.err <- struct{ err error }{nil}
}

// WithCaptcha for now returns error whenever identify a captcha on page
func (t TJMS) WithCaptcha(e *colly.HTMLElement) {
	if e.Attr("value") != "" {
		t.err <- struct{ err error }{ErrCaptchaIdentified}
		return
	}
	t.err <- struct{ err error }{nil}
}

// NotFound when no data is found it will return error
func (t *TJMS) NotFound(e *colly.HTMLElement) {
	t.err <- struct{ err error }{ErrNoResult}
}

func (t TJMS) buildAcessingURL(host, coding string) (string, error) {
	base, err := url.Parse(host)
	if err != nil {
		return "", err
	}
	params := url.Values{}

	nums := strings.Split(coding, ".")
	forumNum := nums[len(nums)-1]

	params.Add("conversationId", "")
	params.Add("cbPesquisa", "NUMPROC")
	params.Add("numeroDigitoAnoUnificado", extractUnifiedDigit(coding))
	params.Add("foroNumeroUnificado", forumNum)
	params.Add("uuidCaptcha", "")

	if strings.Contains(host, "cpopg5") {
		params.Add("dadosConsulta.localPesquisa.cdLocal", "-1")
		params.Add("dadosConsulta.tipoNuProcesso", "UNIFICADO")
		params.Add("dadosConsulta.valorConsultaNuUnificado", coding)
		params.Add("dadosConsulta.valorConsulta", "")
	}

	if strings.Contains(host, "cposg5") {
		params.Add("paginaConsulta", "1")
		params.Add("tipoNuProcesso", "UNIFICADO")
		params.Add("localPesquisa.cdLocal", "-1")
		params.Add("dePesquisaNuUnificado", coding)
		params.Add("dePesquisa", "")
	}

	base.RawQuery = params.Encode()
	return base.String(), nil
}
