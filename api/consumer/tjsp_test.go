package consumer

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/suite"
)

type testTjspSuiteCase struct {
	suite.Suite
	processID, firsStageURL, secondStageURL, secondStageURL2 string
}

type consumerMockTest struct {
	errorOnSubscribe bool
}

func (c consumerMockTest) Subscribe(topicName, groupName string, fn func([]byte) error) error {
	if c.errorOnSubscribe {
		return errors.New("error on subscribe")
	}
	return nil
}

func (c consumerMockTest) Error() error {
	return nil
}

func TestTjspCase(t *testing.T) {
	suite.Run(t, new(testTjspSuiteCase))
}

func (suite *testTjspSuiteCase) SetupSuite() {
	suite.processID = "0710802-55.2018.8.02.0001"
	suite.firsStageURL = "https://www2.tjal.jus.br/cpopg/search.do"
	suite.secondStageURL = "https://www2.tjal.jus.br/cposg5/search.do"
	suite.secondStageURL2 = "https://www2.tjal.jus.br/cposgcr/search.do"
}

func (suite testTjspSuiteCase) TestBuildURlBasedOnCodingOfFirstStage() {
	cons := NewTJSP(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.firsStageURL),
	)

	url, err := cons.buildAcessingURL(suite.firsStageURL, suite.processID)
	suite.Require().NoError(err, "couldnt fail on build url based on processID")
	suite.Require().Contains(url, suite.processID, "should contains processID on url construction")
	suite.Require().Contains(url, "dadosConsulta.localPesquisa.cdLocal", "should contains information on querystring")
	suite.Require().Contains(url, "cbPesquisa", "should contains information on querystring of cbPesquisa")
	suite.Require().Contains(url, "NUMPROC", "should contains information on querystring of NUMPROC")
	suite.Require().Contains(url, "dadosConsulta.tipoNuProcesso", "should contains information on querystring of tipoNuProcesso")
	suite.Require().Contains(url, "UNIFICADO", "should contains information on querystring of unification")
	suite.Require().Contains(url, "numeroDigitoAnoUnificado", "should contains information on querystring of numeroDigitoAnoUnificado")
	suite.Require().Contains(url, "foroNumeroUnificado", "should contains information on querystring of foroNumeroUnificado")
	suite.Require().Contains(url, "dadosConsulta.valorConsultaNuUnificado", "should contains info on querystring of dadosConsulta.valorConsultaNuUnificado")
}

func (suite testTjspSuiteCase) TestBuildURLBasedOnCodingOfSecondStage() {
	cons := NewTJSP(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.secondStageURL),
	)

	url, err := cons.buildAcessingURL(suite.secondStageURL, suite.processID)
	suite.Require().NoError(err, "couldnt fail on build url based on processID")

	suite.Require().Contains(url, suite.processID, "should contains processID on url construction")
	suite.Require().Contains(url, "dadosConsulta.localPesquisa.cdLocal", "should contains information on querystring")
	suite.Require().Contains(url, "cbPesquisa", "should contains information on querystring of cbPesquisa")
	suite.Require().Contains(url, "NUMPROC", "should contains information on querystring of NUMPROC")

	suite.Require().NotContains(url, "dadosConsulta.tipoNuProcesso", "should not contains information on querystring of dadosConsulta.tipoNuProcesso")
	suite.Require().Contains(url, "tipoNuProcesso", "should contains information on querystring of tipoNuProcesso")

	suite.Require().Contains(url, "UNIFICADO", "should contains information on querystring of unification")

	suite.Require().Contains(url, "numeroDigitoAnoUnificado", "should contains information on querystring of numeroDigitoAnoUnificado")
	suite.Require().Contains(url, "foroNumeroUnificado", "should contains information on querystring of foroNumeroUnificado")
	suite.Require().Contains(url, "dePesquisaNuUnificado", "should contains information on querystring of dePesquisaNuUnificado")
	suite.Require().Contains(url, "paginaConsulta", "should contains information on querystring of paginaConsulta")

	suite.Require().NotContains(url, "dadosConsulta.valorConsultaNuUnificado", "should not contains info on querystring of dadosConsulta.valorConsultaNuUnificado")
}

func generateMapResult() map[string]string {
	rs := make(map[string]string)
	rs["Distribuição:"] = "26/05/2015 às 18:31 - Livre"
	rs["Juiz:"] = "Márcia Blanes"
	rs["Valor da ação:"] = "R$ 866.000,00"
	rs["Classe:"] = "Procedimento Comum Cível"
	rs["Processo:"] = "1002298-86.2015.8.26.0271"
	rs["Assunto:"] = "Franquia"
	rs["setor"] = "2ª Vara Cível - Foro de Itapevi"
	return rs
}

func (suite testTjspSuiteCase) TestSecondStageShouldReturnNoDataForThisProcess() {
	cons := NewTJSP(
		consumerMockTest{},
		NumOfCrawlers(1),
		URLVisitors(suite.firsStageURL, suite.secondStageURL, suite.secondStageURL2),
	)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	err := cons.SecondStage(ctx, suite.processID)
	suite.Require().Error(err, "should NOT return error from SECOND stage")
	suite.Require().EqualError(err, "process does not have any data on this stage")
}

func (suite testTjspSuiteCase) TestStartMultiplesSubscriberWithoutError() {
	cons := NewTJSP(
		consumerMockTest{
			errorOnSubscribe: false,
		},
		NumOfCrawlers(5),
		URLVisitors(suite.firsStageURL, suite.secondStageURL, suite.secondStageURL2),
	)

	err := cons.Start()
	suite.Require().NoError(err, "should no fail to start a crawler subscription")
}

func (suite testTjspSuiteCase) TestStartMultiplesSubscriberWithError() {
	cons := NewTJSP(
		consumerMockTest{
			errorOnSubscribe: true,
		},
		NumOfCrawlers(5),
		URLVisitors(suite.firsStageURL, suite.secondStageURL, suite.secondStageURL2),
	)

	err := cons.Start()
	suite.Require().Error(err, "whenever a start fail should return error right after")
}
