package consumer

import (
	"testing"

	"github.com/stretchr/testify/suite"
)

type testHelpersSuiteCase struct {
	suite.Suite
}

func TestHelpersCase(t *testing.T) {
	suite.Run(t, new(testHelpersSuiteCase))
}

func (suite testHelpersSuiteCase) TestNormalizePrice() {
	price, err := normalizePrice("R$ 866.000,00")
	suite.Require().NoError(err, "should not fail to normalize price")
	suite.Require().Equal(866000.00, price, "both price should be equal")
}

func (suite testHelpersSuiteCase) TestNormalizePriceWithErrWithNoSpaceBetweenPrices() {
	_, err := normalizePrice("R$866000,00")
	suite.Require().Error(err, "should fail to normalize price whenever there is no space on price")
}

func (suite testHelpersSuiteCase) TestNormalizePriceWithErrNotNumberConvertion() {
	_, err := normalizePrice("R$ trinta e três mil ")
	suite.Require().Error(err, "should fail to normalize price whenever num is a not valid")
}

func (suite testHelpersSuiteCase) TestDistributionDate() {
	dsDate := "26/05/2015 às 18:31 - Livre"
	date, err := normalizedistributionDate(dsDate)
	suite.Require().NoError(err, "should not fail to normalize date")
	format := date.Format("02/01/2006 15:04:05")
	expected := "26/05/2015 18:31:00"
	suite.Require().Equal(expected, format, "both date in format string should match")
}

func (suite testHelpersSuiteCase) TestDistributionDateWithErrWithDifferentLayout() {
	dsDate := "26-05-2015 às 18:31 - Livre"
	_, err := normalizedistributionDate(dsDate)
	suite.Require().Error(err, "should fail to normalize date with different layout")
}

func (suite testHelpersSuiteCase) TestDistributionDateWithErrWithChanginOnDataStringFormat() {
	dsDate := "26/05/2015 18:31 - Livre"
	_, err := normalizedistributionDate(dsDate)
	suite.Require().Error(err, "should fail to normalize date whenever date format is different from 26/05/2015 18:31")
}

func (suite testHelpersSuiteCase) TestExtractForumNum() {
	result := extractForumNum("1002298-86.2015.8.26.0271")
	suite.Require().Equal("271", result, "both num should be equal - 271")
}

func (suite testHelpersSuiteCase) TestExtractForumNumTJMSShouldFail() {
	result := extractForumNum("0821901-51.2018.8.12.0001")
	suite.Require().NotEqual("0001", result, "result should fail because is 001 instead of 0001")
}

// TODO extractMembers tests

func (suite testHelpersSuiteCase) TestExtractMembers() {
	data := [][]string{
		[]string{
			"DOLCE DUO COMERCIO VAREJISTA DE DOCES LTDA ME e outra ",
			" ",
			"Advogada: Karina de Almeida Batistuci",
			"Agravante:",
		},
		[]string{
			"REBELLOS COMERCIO VAREJISTA DOCES BALAS BOMBONS LTDA  ",
			"  ",
			"  ",
			"Advogada: Karina de Almeida Batistuci",
			"Agravante:",
		},
		[]string{
			"FRANQUIA SHOW ASSESSORIA EM NEGOCIOS LTDA E OUTRA ",
			"Agravado:",
		},
		[]string{
			"IBAC INDÚSTRIA BRASILEIRA DE ALIMENTOS E CHOCOLATES LTDA ",
			"Agravado:",
		},
	}
	members, err := extractProcessMembers(data)
	suite.Require().NoError(err)
	suite.Require().Equal("FRANQUIA SHOW ASSESSORIA EM NEGOCIOS LTDA E OUTRA", members.Defendant[0].Name, "both should be same")
	suite.Require().Len(members.Defendant, 2, "Should have 2 defendants")
	suite.Require().Equal("IBAC INDÚSTRIA BRASILEIRA DE ALIMENTOS E CHOCOLATES LTDA", members.Defendant[1].Name, "both should be same")

	suite.Require().Equal("Advogada: Karina de Almeida Batistuci", members.Accuser[0].Lawyers[0].Name)
	suite.Require().Equal("DOLCE DUO COMERCIO VAREJISTA DE DOCES LTDA ME e outra", members.Accuser[0].Name)
	suite.Require().Equal("REBELLOS COMERCIO VAREJISTA DOCES BALAS BOMBONS LTDA", members.Accuser[1].Name)
	suite.Require().Equal("Advogada: Karina de Almeida Batistuci", members.Accuser[0].Lawyers[0].Name)
	suite.Require().Equal("F", members.Accuser[0].Lawyers[0].Gender)
	suite.Require().Equal("Advogada: Karina de Almeida Batistuci", members.Accuser[1].Lawyers[0].Name)
	suite.Require().Equal("F", members.Accuser[1].Lawyers[0].Gender)
}

func (suite testHelpersSuiteCase) TestExtractMemberWithReprtateOnIt() {
	data := [][]string{
		[]string{
			"DOLCE DUO COMÉRCIO VAREJISTA DE DOCES BALAS BOMBONS E SEMELHANTES LTDA ME",
			"Advogada:  Paula Rodrigues da Silva",
			"Advogado:  Diego Gomes Dias  ",
			"Advogada:  Karina de Almeida Batistuci",
			"Reprtate:  MARIANA REBELLO MARQUES DA COSTA SANTOS ",
			"Reqte:",
		},
		[]string{
			"REBELLOS COMÉRCIO VAREJISTA DOCES BALAS BOMBONS LTDA ",
			"  ",
			"Advogada:  Paula Rodrigues da Silva  ",
			"Reprtate:  MARIANA REBELLO MARQUES DA COSTA SANTOS ",
			"Reqte:",
		},
		[]string{
			"MARIANA REBELLO MARQUES DA COSTA SANTOS ",
			"  ",
			"  ",
			"Advogada:  Paula Rodrigues da Silva ",
			"Reqte:",
		},
		[]string{
			"FÁBIO FURQUIM DA COSTA SANTOS",
			"Advogada:  Paula Rodrigues da Silva",
			"Reqte:",
		},
		[]string{
			"YARA SILVIA REBELLO",
			"Advogada:  Paula Rodrigues da Silva",
			"Reqte:",
		},
		[]string{
			"FRANQUIA SHOW ASSESSORIA EM NEGÓCIOS LTDA",
			"  ",
			"Advogado:  Andre Boschetti Oliva",
			"Reqdo:",
		},
		[]string{
			"IBAC INDÚSTRIA BRASILEIRA DE ALIMENTOS E CHOCOLATES LTDA",
			"Advogado:  Andre Boschetti Oliva",
			"Reqdo:",
		},
	}

	members, err := extractProcessMembers(data)
	suite.Require().NoError(err, "should fail to extract members from legal process")

	// defendant
	suite.Require().Len(members.Defendant, 2, "should have to on defendant member")
	suite.Require().Equal("FRANQUIA SHOW ASSESSORIA EM NEGÓCIOS LTDA", members.Defendant[0].Name, "should match name with expected one")
	suite.Require().Equal("IBAC INDÚSTRIA BRASILEIRA DE ALIMENTOS E CHOCOLATES LTDA", members.Defendant[1].Name, "should mathc name with expected one")
	suite.Require().Empty(members.Defendant[0].Representer, "should not have representer")
	suite.Require().Empty(members.Defendant[1].Representer, "should not have representer")
	suite.Require().Equal("Advogado:  Andre Boschetti Oliva", members.Defendant[0].Lawyers[0].Name, "should be equal Advogado:  Andre Boschetti Oliva")
	suite.Require().Equal("M", members.Defendant[0].Lawyers[0].Gender, "gender should be M")

	// accuser
	suite.Require().Len(members.Accuser, 5, "should have 5")
	suite.Require().Len(members.Accuser[0].Lawyers, 3, "should have 3 lawyers")
	suite.Require().Equal("DOLCE DUO COMÉRCIO VAREJISTA DE DOCES BALAS BOMBONS E SEMELHANTES LTDA ME", members.Accuser[0].Name, "should match with expected one ")
	suite.Require().Equal("Reprtate:  MARIANA REBELLO MARQUES DA COSTA SANTOS", members.Accuser[0].Representer)
}

func (suite testHelpersSuiteCase) TestExtractMemberFromTJMS() {
	data := [][]string{
		[]string{
			"Estado de Mato Grosso do Sul",
			"   ",
			"   ",
			"   ",
			"   ",
			"   ",
			"RepreLeg:",
			"Procuradoria Geral do Estado de Mato Grosso do Sul",
			"Réu",
		},
	}
	members, err := extractProcessMembers(data)
	suite.Require().NoError(err)
	suite.Require().Len(members.Accuser, 0, "should not have accusers")
	suite.Require().Len(members.Defendant, 1, "should  have 1 defendant")
	suite.Require().Equal(members.Defendant[0].Representer, "Procuradoria Geral do Estado de Mato Grosso do Sul", "representer should match expectation")
	suite.Require().Equal(members.Defendant[0].Name, "Estado de Mato Grosso do Sul", "name should match expectation")
}

func (suite testTJMSSuiteCase) TestExtactMemberFromTJMSFullData() {
	data := [][]string{
		[]string{
			"Leidi Silva Ormond Galvão",
			"Advogada:",
			"Adriana Catelan Skowronski",
			"Advogada:",
			"Ana Silvia Pessoa Salgado de Moura",
			"Autora:",
		},
		[]string{
			"Melissa Chaves Miranda",
			"Advogada:",
			"Adriana Catelan Skowronski",
			"Advogada:",
			"Ana Silvia Pessoa Salgado de Moura",
			"Autora:",
		},
		[]string{
			"Ruzymar Campos de Oliveira",
			"Advogada:",
			"Adriana Catelan Skowronski",
			"Advogada:",
			"Ana Silvia Pessoa Salgado de Moura",
			"Autora:",
		},
		[]string{
			"Estado de Mato Grosso do Sul",
			"RepreLeg:",
			"Procuradoria Geral do Estado de Mato Grosso do Sul",
			"Réu:",
		},
	}
	members, err := extractProcessMembers(data)
	suite.Require().NoError(err)
	suite.Require().Len(members.Accuser, 3, "should be 3 accusers")
	suite.Require().Len(members.Defendant, 1, "should be 1  defendant")
	suite.Require().Equal("Estado de Mato Grosso do Sul", members.Defendant[0].Name, "should match both string")
	suite.Require().Equal("Procuradoria Geral do Estado de Mato Grosso do Sul", members.Defendant[0].Representer, "should match both string")
}

func (suite testHelpersSuiteCase) TestPatternMatchDefendant() {
	data := "Agravado:"
	suite.Require().True(patternMatchingDefendant(data))

	data = "Reqdo:"
	suite.Require().True(patternMatchingDefendant(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchDefendantWithLowerCase() {
	data := "agravado:"
	suite.Require().True(patternMatchingDefendant(data))

	data = "reqdo:"
	suite.Require().True(patternMatchingDefendant(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchDefendantUpperCase() {
	data := "AGRAVADO:"
	suite.Require().True(patternMatchingDefendant(data))
	data = "REQDO:"
	suite.Require().True(patternMatchingDefendant(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchDefendantShouldNotFailIfNotContainsDots() {
	data := "Agravado"
	suite.Require().True(patternMatchingDefendant(data), "should contain : after str")

	data = "Reqdo"
	suite.Require().True(patternMatchingDefendant(data), "should contain : after str")
}

func (suite testHelpersSuiteCase) TestPatternMatchAccuser() {
	data := "Agravante:"
	suite.Require().True(patternMatchingAccuser(data))

	data = "Reqte:"
	suite.Require().True(patternMatchingAccuser(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchAccuserWithLowerCase() {
	data := "agravante:"
	suite.Require().True(patternMatchingAccuser(data))

	data = "reqte:"
	suite.Require().True(patternMatchingAccuser(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchAccuserUpperCase() {
	data := "AGRAVANTE:"
	suite.Require().True(patternMatchingAccuser(data))
	data = "REQTE:"
	suite.Require().True(patternMatchingAccuser(data))
}

func (suite testHelpersSuiteCase) TestPatternMatchAccuserFail() {
	data := "agravante"
	suite.Require().False(patternMatchingAccuser(data), "should contain : after str")

	data = "REQTE"
	suite.Require().False(patternMatchingAccuser(data), "should contain : after str")
}

func (suite testHelpersSuiteCase) TestExtractUnifiedDigiteFromTJSPNum() {
	processID := "1002298-86.2015.8.26.0271"
	expected := "1002298-86.2015"

	result := extractUnifiedDigit(processID)
	suite.Require().Equal(expected, result, "both string does not match")
}

func (suite testHelpersSuiteCase) TestExtractUnifiedDigiteFromTJMSNum() {
	processID := "0821901-51.2018.8.12.0001 "
	expected := "0821901-51.2018"

	result := extractUnifiedDigit(processID)
	suite.Require().Equal(expected, result, "both string does not match")
}

func (suite testHelpersSuiteCase) TestPatternMatchingRepresentarSuccessfully() {
	suite.Require().True(patternMatchingRepresenter("reprtate:"))
	suite.Require().True(patternMatchingRepresenter("repreleg:"))
}

func (suite testHelpersSuiteCase) TestPatternMatchingRepresentarUpperCaseSuccessfully() {
	suite.Require().True(patternMatchingRepresenter("Reprtate:"))
	suite.Require().True(patternMatchingRepresenter("Repreleg:"))
	suite.Require().True(patternMatchingRepresenter("REPRTATE:"))
	suite.Require().True(patternMatchingRepresenter("REPRELEG:"))
}

func (suite testHelpersSuiteCase) TestPatternMatchingRepresentarNotMatching() {
	suite.Require().False(patternMatchingRepresenter("Reprtate"))
	suite.Require().False(patternMatchingRepresenter("Repreleg"))
	suite.Require().False(patternMatchingRepresenter("REPRTATE"))
	suite.Require().False(patternMatchingRepresenter("REPRELEG"))
}

func (suite testHelpersSuiteCase) TestMatchingPreviusIdentificationOfRepORLawyer() {
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("Reprtate:"))
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("Repreleg:"))
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("advogada:"))
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("advogado:"))
}

func (suite testHelpersSuiteCase) TestMatchingPreviusIdentificationOfRepORLawyerFalse() {
	suite.Require().False(patternMatchingPreviusIsReprOrLawyer(""))
	suite.Require().False(patternMatchingPreviusIsReprOrLawyer("teste"))
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("advogada"))
	suite.Require().True(patternMatchingPreviusIsReprOrLawyer("advogado"))
}

func (suite testHelpersSuiteCase) TestLawyerGenderFemale() {
	suite.Require().Equal("F", patternMatchingGenderLawyer("advogada:"))
	suite.Require().Equal("F", patternMatchingGenderLawyer("advogada"))
	suite.Require().Equal("F", patternMatchingGenderLawyer("advogada - "))
	suite.Require().Equal("F", patternMatchingGenderLawyer("advogada &*"))
	suite.Require().Equal("F", patternMatchingGenderLawyer("ADVOGADA:"))
	suite.Require().Equal("F", patternMatchingGenderLawyer("AdVoGaDa:"))
}

func (suite testHelpersSuiteCase) TestLawyerGenderMale() {
	suite.Require().Equal("M", patternMatchingGenderLawyer("advogado:"))
	suite.Require().Equal("M", patternMatchingGenderLawyer("advogado"))
	suite.Require().Equal("M", patternMatchingGenderLawyer("advogado - "))
	suite.Require().Equal("M", patternMatchingGenderLawyer("advogado &*"))
	suite.Require().Equal("M", patternMatchingGenderLawyer("ADVOGADO:"))
	suite.Require().Equal("M", patternMatchingGenderLawyer("AdVoGaDo:"))
}

func (suite testHelpersSuiteCase) TestLawyerGenderUnknwon() {
	suite.Require().Equal("", patternMatchingGenderLawyer("outros:"))
	suite.Require().Equal("", patternMatchingGenderLawyer(""))
}

func (suite testHelpersSuiteCase) TestLawyerNameOnNextLine() {
	suite.Require().True(patternMatchingLaywerNameOnNextLine("advogado:"))
	suite.Require().True(patternMatchingLaywerNameOnNextLine("advogada:"))
}

func (suite testHelpersSuiteCase) TestLawyerNameNotOnNextLine() {
	suite.Require().False(patternMatchingLaywerNameOnNextLine("advogado: Luiz Roberto"))
	suite.Require().False(patternMatchingLaywerNameOnNextLine("advogada: Alessandra Pereira"))
}
