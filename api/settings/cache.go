package settings

import "time"

//Cache settings
type Cache struct {
	client `mapstructure:",squash"`
}

type client struct {
	Network            string        `mapstructure:"network"`
	Addr               string        `mapstructure:"addr"`
	Db                 int           `mapstructure:"db"`
	Pswd               string        `mapstructure:"password"`
	MaxRetries         int           `mapstructure:"max_retries"`
	MinRetryBackoff    time.Duration `mapstructure:"min_retry_backoff"`
	MaxRetryBackoff    time.Duration `mapstructure:"max_retry_backoff"`
	DialTimeout        time.Duration `mapstructure:"dial_timeout"`
	ReadTimeout        time.Duration `mapstructure:"read_timeout"`
	WriteTimeout       time.Duration `mapstructure:"write_timeout"`
	PoolsizePerCluster int           `mapstructure:"pool_size_per_cluster"`
	MinIdleConns       int           `mapstructure:"min_idle_conns"`
	MaxConnAge         time.Duration `mapstructure:"max_conn_age"`
	PoolTimeout        time.Duration `mapstructure:"pool_timeout"`
	IddleTimeout       time.Duration `mapstructure:"iddle_timeout"`
	IdleCheckFrequency time.Duration `mapstructure:"idle_check_frequency"`
	ReadOnly           bool          `mapstructure:"read_only"`
}

type cluster struct {
	Addrs          []string `mapstructure:"addrs"`
	MaxRedirects   int      `mapstructure:"max_redirects"`
	RouteByLatency bool     `mapstructure:"router_by_latency"`
	RouteRandomly  bool     `mapstructure:"router_randomly"`
	client
}
