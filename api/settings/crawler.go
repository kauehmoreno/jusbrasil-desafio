package settings

import "time"

//crawlerSettings config of every crawler
type crawlerSettings struct {
	Name          string        `mapstructure:"name"`
	Host          string        `mapstructure:"host"`
	URLS          []string      `mapstructure:"urls"`
	Timeout       time.Duration `mapstructure:"timeout"`
	NumOfCrawlers int           `mapstructure:"num_of_crawlers"`
}

// TJAL TJAL config
type TJAL struct {
	crawlerSettings `mapstructure:",squash"`
}

// TJMS TJMS config
type TJMS struct {
	crawlerSettings `mapstructure:",squash"`
}

type Crawler struct {
	TJAL `mapstructure:"tjal"`
	TJMS `mapstructure:"tjms"`
}
