package settings

import "time"

// DatabaseSettings set all database access config
type DatabaseSettings struct {
	ConnMaxLifetime time.Duration `mapstructure:"conn_max_lifetime"`
	Host            string        `mapstructure:"host"`
	MaxConn         int           `mapstructure:"max_conn"`
	MaxIdle         time.Duration `mapstructure:"max_idle"`
	MaxPoolSize     uint16        `mapstructure:"max_poolsize"`
	Name            string        `mapstructure:"name"`
	Password        string        `mapstructure:"password"`
	Table           Table         `mapstructure:"table"`
	Timeout         string        `mapstructure:"timeout"`
	User            string        `mapstructure:"user"`
	Port            string        `mapstructure:"port"`
	ConnTimeout     time.Duration `mapstructure:"conn_timeout"`
}

// DB expose config from both read and write DB
type DB struct {
	Read  DatabaseSettings `mapstructure:"read"`
	Write DatabaseSettings `mapstructure:"write"`
}

//Job table name
type Job struct {
	Table string `mapstructure:"name"`
}

//LegalProcess table name
type LegalProcess struct {
	Table string `mapstructure:"name"`
}

// Table define tables names of database
type Table struct {
	Job          Job          `mapstructure:"job"`
	LegalProcess LegalProcess `mapstructure:"legal_process"`
}
