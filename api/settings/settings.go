package settings

import "time"

type Settings struct {
	Cors    Cors    `mapstructure:"cors"`
	DB      DB      `mapstructure:"db"`
	Cache   Cache   `mapstructure:"cache"`
	Crawler Crawler `mapstructure:"crawlers"`
	base    `mapstructure:",squash"`
}

type base struct {
	Ambiente             string        `mapstructure:"ambiente"`
	Host                 string        `mapstructure:"host"`
	Port                 string        `mapstructure:"port"`
	Service              string        `mapstructure:"service"`
	DefaultHandleTimeout time.Duration `mapstructure:"default_timeout"`
	ReadTimeout          time.Duration `mapstructure:"read_timeout"`
	WriteTimeout         time.Duration `mapstructure:"write_timeout"`
	IdleTimeout          time.Duration `mapstructure:"idle_timeout"`
	Token                string        `mapstructure:"token"`
	QueueHost            string        `mapstructure:"queue_host"`
}
