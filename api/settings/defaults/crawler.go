package defaults

import (
	"time"

	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("tjal.name", "TJAL")
	viper.SetDefault("tjal.host", "https://www2.tjal.jus.br")
	viper.SetDefault("tjal.urls", []string{"https://www2.tjal.jus.br/cpopg/search.do", "https://www2.tjal.jus.br/cposg5/search.do"})
	viper.SetDefault("tjal.timeout", time.Second*10)
	viper.SetDefault("tjal.num_of_crawlers", 10)

	viper.SetDefault("tjms.name", "TJMS")
	viper.SetDefault("tjms.host", " https://esaj.tjms.jus.br")
	viper.SetDefault("tjms.urls", []string{"https://esaj.tjms.jus.br/cpopg5/search.do", "https://esaj.tjms.jus.br/cposg5/search.do"})
	viper.SetDefault("tjms.timeout", time.Second*10)
	viper.SetDefault("tjms.num_of_crawlers", 10)
}
