package defaults

import (
	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("cors.allowed", []string{"*"})
	viper.SetDefault("cors.allowed_methods", []string{"GET", "POST"})
	viper.SetDefault("cors.allowed_headers", []string{"Accept", "Content-Type"})
}
