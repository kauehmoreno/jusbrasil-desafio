package defaults

import (
	"github.com/spf13/viper"
)

func init() {
	viper.SetDefault("ambiente", "local")
	viper.SetDefault("host", "http://localhost:8000/")
	viper.SetDefault("port", "8000")
	viper.SetDefault("service", "backend")
	viper.SetDefault("token", "amFjb3J0ZWlhcHA=/UktKKjFhcHBSZWFjdG5hdGl2ZQ==")
	viper.SetDefault("default_timeout", "3s")
	viper.SetDefault("queue_host", "nats://queue:4222")
}
