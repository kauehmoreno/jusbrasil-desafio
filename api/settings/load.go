package settings

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var settingsMapper *Settings

func init() {
	settingsMapper = &Settings{}
	load(settingsMapper)
}

func Get() *Settings {
	return settingsMapper
}

func load(settings *Settings) {
	var conf string
	if conf = os.Getenv("JACORTEI_API_CONFIG"); conf == "" {
		conf = "deploy/settings_local.json"
	}

	if err := readerBackOff(4, conf); err != nil {
		log.Fatalf("[settings.init] could not load settings file %v", err)
	}

	if err := viper.Unmarshal(settings); err != nil {
		log.Fatalf("[settings.init] couldn't unmarshal settings %v", err)
	}
}

func readerBackOff(try int, conf string) error {
	var (
		count int
		err   error
	)
	for count <= try {
		err = func(conf string) error {
			viper.SetConfigFile(conf)
			return viper.ReadInConfig()
		}(conf)
		if err != nil {
			count++
			log.Errorf("[settings.init] error on read file it might not exist - retrying with different path %s", conf)
			conf = fmt.Sprintf("../%s", conf)
			continue
		}
		break
	}
	return err
}
