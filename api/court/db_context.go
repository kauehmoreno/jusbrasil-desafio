package court

import (
	"context"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"
)

type writeRepository struct {
	storage cache.Cacheable
	db      db.Writer
}

// Save any legal process into DB and Cache
func (wr writeRepository) Save(ctx context.Context, p Process) error {
	ch := make(chan struct{ err error })
	go func() {
		err := wr.db.Insert(ctx, settings.Get().DB.Write.Table.LegalProcess.Table, p)
		ch <- struct{ err error }{err}
	}()

	select {
	case <-ctx.Done():
		return ctx.Err()
	case result := <-ch:
		if result.err != nil {
			return result.err
		}
		key := cache.FormatKey(cache.LegalProcessID, p.LegalProcess.ID)
		defer wr.storage.Set(key, p, cache.OneMonth)
		return nil
	}
}

type readerRepository struct {
	storage          cache.Cacheable
	listeableStorage cache.Listeable
	db               db.Reader
}

// ByID return a process byID
func (rr readerRepository) ByID(ctx context.Context, id string) (Process, error) {
	key := cache.FormatKey(cache.LegalProcessID, id)
	var legalProcess Process
	if err := rr.storage.Get(key, &legalProcess); err != nil {
		err = rr.db.Get(
			ctx, settings.Get().DB.Read.Table.LegalProcess.Table,
			bson.D{{"id", id}}, &legalProcess)
		if err != nil {
			return legalProcess, err
		}
		defer rr.storage.Set(key, legalProcess, cache.OneMonth)
	}
	return legalProcess, nil
}

// ByCourt return a list of process based on court params
func (rr readerRepository) ByCourt(ctx context.Context, court string, limit int64) ([]Process, error) {
	key := cache.FormatKey(cache.LegalProcessByCourt, court)
	exist, _ := rr.existOnCacheList(key)
	if !exist {
		process, err := rr.byCourtDB(ctx, court, limit)
		if err != nil {
			return []Process{}, err
		}
		return process, nil
	}

	ids, err := rr.listeableStorage.Range(key, 0, limit)
	if err != nil {
		return []Process{}, err
	}

	var result []Process
	for _, id := range ids {
		p, err := rr.ByID(ctx, id)
		if err != nil {
			return []Process{}, err
		}
		result = append(result, p)
	}
	return result, nil
}

// byCourtDB look for legal process on db and update cache list ids representation
func (rr readerRepository) byCourtDB(ctx context.Context, court string, limit int64) ([]Process, error) {
	var result []Process
	err := rr.db.Filter(
		ctx, settings.Get().DB.Read.Table.LegalProcess.Table,
		bson.D{{"court", court}},
		&result)
	if err != nil {
		return result, err
	}
	key := cache.FormatKey(cache.LegalProcessByCourt, court)
	for _, p := range result {
		rr.listeableStorage.Push(key, p.LegalProcess.ID)
	}
	return result, nil
}

// existOnCacheList simple way to check if there is a cache list key
func (rr readerRepository) existOnCacheList(key string) (bool, error) {
	length, err := rr.listeableStorage.Len(key)
	if err != nil {
		return false, err
	}
	return length > 0, nil
}

// ByProcessID return legal process based on processID
func (rr readerRepository) ByProcessID(ctx context.Context, processID string) ([]Process, error) {
	key := cache.FormatKey(cache.LegalProcessID, processID)
	var rs []Process
	if err := rr.storage.Get(key, &rs); err != nil {
		erro := rr.db.Filter(
			ctx, settings.Get().DB.Read.Table.LegalProcess.Table,
			bson.D{{"legal_process.process_id", processID}}, &rs)
		if erro != nil {
			return rs, erro
		}
		defer rr.storage.Set(key, rs, cache.OneHour)
	}
	return rs, nil
}

//Repository compose both read and write operation
// every package cli will receive a repository instance to interact with both cache and db
type Repository struct {
	writeRepository
	readerRepository
}

// Options allow client to customize instance options
// make test easier
type Options func(*Repository)

//Cacheable allow cli to customize it cache implementation
func Cacheable(c cache.Cacheable) Options {
	return func(r *Repository) {
		r.writeRepository.storage = c
		r.readerRepository.storage = c
	}
}

//Listeable allow cli to customize it listeadble implementation
func Listeable(l cache.Listeable) Options {
	return func(r *Repository) {
		r.listeableStorage = l
	}
}

// Database allow client to customize it on database
func Database(db db.DB) Options {
	return func(r *Repository) {
		r.readerRepository.db = db
		r.writeRepository.db = db
	}
}

//NewManager func return a repository instance to cli or error if something fail
// as connect to database
func NewManager(opts ...Options) (Repository, error) {
	var rep Repository
	for _, opt := range opts {
		opt(&rep)
	}
	return rep, nil
}
