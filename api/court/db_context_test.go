package court_test

import (
	"context"
	"errors"
	"testing"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"

	"github.com/stretchr/testify/mock"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/court"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/mocks"

	"github.com/stretchr/testify/suite"
)

type testCourtDbContextSuiteCase struct {
	suite.Suite
	fakeID string
	*mocks.MockCacheable
	*mocks.MockDB
	*mocks.MockListeable
}

func TestCourtDbContextCase(t *testing.T) {
	suite.Run(t, new(testCourtDbContextSuiteCase))
}

func (suite *testCourtDbContextSuiteCase) SetupSuite() {
	suite.fakeID = core.GenerateUUID()
	// suite.MockCacheable = new(mocks.MockCacheable)
	// suite.MockDB = new(mocks.MockDB)
	// suite.MockListeable = new(mocks.MockListeable)
}

func (suite *testCourtDbContextSuiteCase) SetupTest() {
	suite.MockCacheable = new(mocks.MockCacheable)
	suite.MockDB = new(mocks.MockDB)
	suite.MockListeable = new(mocks.MockListeable)
}

func (suite testCourtDbContextSuiteCase) TestSaveWithoutErrorShouldExecuteCacheAfterwards() {
	ctx := context.Background()
	lp := court.NewLegalProcess("tjsp")
	suite.MockDB.On("Insert", ctx, mock.Anything, lp).Return(nil)
	key := cache.FormatKey(cache.LegalProcessID, lp.LegalProcess.ID)
	suite.MockCacheable.On("Set", key, lp, cache.OneMonth).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Database(suite.MockDB))

	suite.Require().NoError(err, "should not fail on get manager instance")

	err = manager.Save(ctx, lp)
	suite.MockCacheable.AssertNumberOfCalls(suite.T(), "Set", 1)
	suite.MockCacheable.AssertCalled(suite.T(), "Set", key, lp, cache.OneMonth)
	suite.Require().NoError(err, "should not fail on save process into db")
}

func (suite testCourtDbContextSuiteCase) TestSaveWithErrorShouldNoCallCache() {
	ctx := context.Background()
	lp := court.NewLegalProcess("tjsp")
	suite.MockDB.On("Insert", ctx, mock.Anything, lp).Return(errors.New("CUSTOM ERROR"))

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	err = manager.Save(ctx, lp)
	suite.Require().Error(err, "should return error on save whenever a database fail")
	suite.MockCacheable.AssertNotCalled(suite.T(), "Set", mock.Anything, mock.Anything, mock.Anything)
}

func (suite testCourtDbContextSuiteCase) TestSaveWithErrorOnCacheShouldNotAffectReturningValue() {
	ctx := context.Background()
	lp := court.NewLegalProcess("tjsp")
	suite.MockDB.On("Insert", ctx, mock.Anything, lp).Return(nil)
	key := cache.FormatKey(cache.LegalProcessID, lp.LegalProcess.ID)
	suite.MockCacheable.On("Set", key, lp, cache.OneMonth).Return(errors.New("CUSTOM ERROR"))

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	err = manager.Save(ctx, lp)
	suite.Require().NoError(err, "should not return error even though cache has failed")
}

func (suite testCourtDbContextSuiteCase) TestByIDShouldLookOnCacheFirstAndNotExecDBIfExist() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessID, suite.fakeID)
	suite.MockCacheable.On("Get", key, mock.Anything).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByID(ctx, suite.fakeID)
	suite.Require().NoError(err, "should not contains error on byID return value")
}

func (suite testCourtDbContextSuiteCase) TestByIDShouldLookOnDBWhenDoesNotFindOnCacheAndThenHitCache() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessID, suite.fakeID)
	suite.MockCacheable.On("Get", key, mock.Anything).Return(errors.New("Nil"))
	suite.MockCacheable.On("Set", key, mock.Anything, cache.OneMonth).Return(nil)

	suite.MockDB.On("Get", ctx, mock.Anything, bson.D{{"id", suite.fakeID}}, mock.Anything).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByID(ctx, suite.fakeID)
	suite.MockDB.AssertNumberOfCalls(suite.T(), "Get", 1)
	suite.MockCacheable.AssertNumberOfCalls(suite.T(), "Set", 1)
	suite.MockDB.AssertCalled(suite.T(), "Get", ctx, mock.Anything, bson.D{{"id", suite.fakeID}}, mock.Anything)
	suite.Require().NoError(err, "should not return error")
}

func (suite testCourtDbContextSuiteCase) TestByCourtShouldLookForCacheReferencesIDAndThenGoForEachIDWhenCacheIsPopulated() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessByCourt, "tjsp")

	suite.MockListeable.On("Len", key).Return(3, nil)
	suite.MockListeable.On("Range", key, int64(0), int64(3)).Return([]string{"1", "2", "3"}, nil)

	suite.MockCacheable.On("Get", mock.Anything, mock.Anything).Times(3).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByCourt(ctx, "tjsp", 3)
	suite.Require().NoError(err, "should not return error from get byCourt")
	suite.MockListeable.AssertNumberOfCalls(suite.T(), "Range", 1)
	suite.MockListeable.AssertNumberOfCalls(suite.T(), "Len", 1)

	suite.MockCacheable.AssertNumberOfCalls(suite.T(), "Get", 3)
}

func (suite testCourtDbContextSuiteCase) TestByCourtGoForDBWhenDoesNotHaveOnCache() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessByCourt, "tjsp")

	suite.MockListeable.On("Len", key).Return(0, nil)

	suite.MockDB.On("Filter", ctx, mock.Anything, bson.D{{"court", "tjsp"}}, mock.Anything).Return(nil)

	suite.MockListeable.On("Push", key, mock.Anything).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByCourt(ctx, "tjsp", 3)
	suite.Require().NoError(err, "should not return error from get byCourt")
	suite.MockDB.AssertCalled(suite.T(), "Filter", ctx, mock.Anything, bson.D{{"court", "tjsp"}}, mock.Anything)
}

func (suite testCourtDbContextSuiteCase) TestByCourtGoForDBWhenDoesNotHaveOnCacheAndThenUpdateCacheList() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessByCourt, "tjsp")

	suite.MockListeable.On("Len", key).Return(0, nil)
	suite.MockDB.On("Filter", ctx, mock.Anything, bson.D{{"court", "tjsp"}}, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		rs := args.Get(len(args) - 1).(*[]court.Process)
		*rs = append(*rs, court.NewLegalProcess("tjsp"))
	})

	suite.MockListeable.On("Push", key, mock.Anything).Return(nil)

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByCourt(ctx, "tjsp", 3)
	suite.Require().NoError(err, "should not return error from get byCourt")
	suite.MockDB.AssertCalled(suite.T(), "Filter", ctx, mock.Anything, bson.D{{"court", "tjsp"}}, mock.Anything)
	suite.MockListeable.AssertNumberOfCalls(suite.T(), "Push", 1)
}

func (suite testCourtDbContextSuiteCase) TestByProcessIDAlwaysLookOnCacheFirst() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessID, suite.fakeID)

	suite.MockCacheable.On("Get", key, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		lps := args.Get(1).(*[]court.Process)
		pr := court.Process{
			LegalProcess: court.LegalProcess{
				ProcessID: suite.fakeID,
			},
		}
		*lps = append(*lps, pr)
	})

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	lp, err := manager.ByProcessID(ctx, suite.fakeID)
	suite.MockDB.AssertNumberOfCalls(suite.T(), "Get", 0)
	suite.Require().NoError(err, "should not fail to retrieve by processID")
	suite.Require().Equal(suite.fakeID, lp[0].LegalProcess.ProcessID, "processID should match")
}

func (suite testCourtDbContextSuiteCase) TestByProcessIDShouldLookOnDBWheneverCacheDoesNotRetrieveValue() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessID, suite.fakeID)

	suite.MockCacheable.On("Get", key, mock.Anything).Return(errors.New("custom error"))
	suite.MockCacheable.On("Set", key, mock.Anything, cache.OneHour).Return(nil)

	suite.MockDB.On("Filter", ctx, mock.Anything, bson.D{{"legal_process.process_id", suite.fakeID}}, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		lps := args.Get(len(args) - 1).(*[]court.Process)
		pr := court.Process{
			LegalProcess: court.LegalProcess{
				ProcessID: suite.fakeID,
			},
		}
		*lps = append(*lps, pr)
	})

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	lp, err := manager.ByProcessID(ctx, suite.fakeID)
	suite.Require().NoError(err, "should not fail to retrieve by processID")
	suite.MockDB.AssertNumberOfCalls(suite.T(), "Filter", 1)
	suite.Require().Equal(suite.fakeID, lp[0].LegalProcess.ProcessID, "processID should match")
}

func (suite testCourtDbContextSuiteCase) TestByProcessIDShoudlFailIfDBReturnErrorAndNotUpdateCacheValue() {
	ctx := context.Background()
	key := cache.FormatKey(cache.LegalProcessID, suite.fakeID)

	suite.MockCacheable.On("Get", key, mock.Anything).Return(errors.New("custom error"))
	suite.MockDB.On("Filter", ctx, mock.Anything, bson.D{{"legal_process.process_id", suite.fakeID}}, mock.Anything).Return(errors.New("custom error"))

	manager, err := court.NewManager(
		court.Cacheable(suite.MockCacheable),
		court.Listeable(suite.MockListeable),
		court.Database(suite.MockDB))
	suite.Require().NoError(err, "should not fail on get manager instance")

	_, err = manager.ByProcessID(ctx, suite.fakeID)
	suite.Require().Error(err, "should fail to retrieve by processID because db fail")
	suite.MockCacheable.AssertNotCalled(suite.T(), "Set", key, mock.Anything, cache.OneMonth)
}
