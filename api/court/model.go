package court

import (
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
)

// Process it will be used to answer back every income legal process action
type Process struct {
	Court        string       `json:"court" bson:"court"`
	LegalProcess LegalProcess `json:"legal_process" bson:"legal_process"`
}

//NewLegalProcess allow cli to make it easier to retrieve
func NewLegalProcess(court string) Process {
	return Process{
		Court: court,
		LegalProcess: LegalProcess{
			ID: core.GenerateUUID(),
		},
	}
}

// LegalProcess a representation of a legal process
type LegalProcess struct {
	ID               string       `json:"id" bson:"id"`
	Stage            int          `json:"stage" bson:"stage"`
	ProcessID        string       `json:"process_id" bson:"process_id"`
	Classe           string       `json:"classe" bson:"classe"`
	Area             string       `json:"area" bson:"area"`
	Issue            string       `json:"issue" bson:"issue"`
	Distribution     Distribution `json:"distribution" bson:"distribution"`
	Judge            string       `json:"judge" bson:"judge"`
	LegalActionPrice float64      `json:"price" bson:"price"`
	Answerable       Answerable   `json:"answerable" bson:"answerable"`
	Stages           []Stage      `json:"legal_process_movements" bson:"legal_process_movements"`
}

type Distribution struct {
	Date time.Time `json:"date" bson:"date"`
	Name string    `json:"name" bson:"name"`
}

// Answerable is all parts of the legal process
type Answerable struct {
	Defendant []Representative `json:"defendants" bson:"defendants"`
	Accuser   []Representative `json:"accusers" bson:"accusers"`
}

// Representative of legal process including lawyer and representer
type Representative struct {
	Lawyers     []Lawyer `json:"lawyers" bson:"lawyers"`
	Representer string   `json:"representer" bson:"representer"`
	Name        string   `json:"name" bson:"name"`
}

// Lawyer describe if is a womem or man and its name
type Lawyer struct {
	Gender string `json:"gender" bson:"gender"`
	Name   string `json:"name" bson:"name"`
}

type LegalProcesStages struct {
	Stages []Stage
}

type Stage struct {
	Date   time.Time `json:"date" bson:"date"`
	Action string    `json:"action" bson:"action"`
}
