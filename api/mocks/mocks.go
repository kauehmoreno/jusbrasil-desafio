package mocks

import (
	"context"
	"net/http"
	"net/http/httptest"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/mock"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
)

func Server(statusCode int, data interface{}, wait time.Duration) *httptest.Server {
	server := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/" {
				time.Sleep(wait)
				defer log.Info("Handler ended")
				log.Info("Handler started")
				core.WriteResponse(w, data, statusCode)
				return
			}
			time.Sleep(wait - wait/2)
			defer log.Info("Cached Handler ended")
			log.Info("Cached Handler started")
			core.WriteCacheResponse(w, data, statusCode, "max-age=100")
			return
		}),
	)
	return server
}

type MockCacheable struct {
	mock.Mock
}

func (m *MockCacheable) Get(s string, v interface{}) error {
	args := m.Called(s, v)
	return args.Error(0)
}

func (m *MockCacheable) Set(s string, v interface{}, t time.Duration) error {
	args := m.Called(s, v, t)
	return args.Error(0)
}
func (m *MockCacheable) Del(k ...string) error {
	args := m.Called(k)
	return args.Error(0)
}

type MockListeable struct {
	mock.Mock
}

func (m *MockListeable) Push(k string, v interface{}) error {
	args := m.Called(k, v)
	return args.Error(0)
}

func (m *MockListeable) Range(key string, from, to int64) ([]string, error) {
	args := m.Called(key, from, to)
	ids := args.Get(0).([]string)
	return ids, args.Error(1)
}
func (m *MockListeable) Rem(k string, v interface{}) error {
	args := m.Called(k, v)
	return args.Error(0)
}
func (m *MockListeable) Len(k string) (int64, error) {
	args := m.Called(k)
	return int64(args.Int(0)), args.Error(1)
}

type MockDB struct {
	mock.Mock
}

func (m *MockDB) Get(c context.Context, t string, q, e interface{}) error {
	args := m.Called(c, t, q, e)
	return args.Error(0)
}
func (m *MockDB) Filter(c context.Context, t string, q, e interface{}) error {
	args := m.Called(c, t, q, e)
	return args.Error(0)
}

func (m *MockDB) Insert(c context.Context, t string, i interface{}) error {
	args := m.Called(c, t, i)
	return args.Error(0)
}

func (m *MockDB) Update(c context.Context, t string, q, d interface{}) error {
	args := m.Called(c, t, q, d)
	return args.Error(0)
}

func (m *MockDB) Delete(c context.Context, t string, q interface{}) error {
	args := m.Called(c, t, q)
	return args.Error(0)
}
