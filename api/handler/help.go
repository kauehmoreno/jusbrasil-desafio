package handler

import (
	"context"
	"net/http"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"
	"go.mongodb.org/mongo-driver/mongo/readpref"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"github.com/julienschmidt/httprouter"
	log "github.com/sirupsen/logrus"
)

//PanicRecovery recovery every panic on api
func PanicRecovery(w http.ResponseWriter, r *http.Request, err interface{}) {
	log.Errorf("[handler.PanicRecovery] Something got wrong and was not expected %v", err)
	core.WriteResponse(w, core.BaseResponse{
		Message: http.StatusText(http.StatusInternalServerError),
		Status:  http.StatusInternalServerError,
	}, http.StatusInternalServerError)
}

func Healthcheck(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	w.Write([]byte("working"))
}

//HealthcheckQueue check queue service
func HealthcheckQueue(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	conn, err := queue.Connect()
	if err != nil {
		core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
		return
	}
	defer conn.Close()
	if !conn.IsConnected() {
		core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
		return
	}
	core.WriteResponse(w, core.BaseResponse{
		Message: "working",
		Status:  http.StatusOK,
	}, http.StatusOK)
}

//HealthcheckCache check cache service
func HealthcheckCache(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if err := cache.New().Ping(); err != nil {
		core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
		return
	}
	core.WriteResponse(w, core.BaseResponse{
		Message: "working",
		Status:  http.StatusOK,
	}, http.StatusOK)
}

//HealthcheckDB check DB service
func HealthcheckDB(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {

	database, err := db.Connect()
	if err != nil {
		core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
		return
	}
	ctx, cancel := context.WithTimeout(r.Context(), time.Millisecond*400)
	defer cancel()

	ch := make(chan struct{ err error })
	go func() {
		err := database.Client().Ping(ctx, readpref.Primary())
		ch <- struct{ err error }{err}
		return
	}()

	select {
	case erro := <-ch:
		if erro.err != nil {
			core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
			return
		}
		core.WriteResponse(w, core.BaseResponse{
			Message: "working",
			Status:  http.StatusOK,
		}, http.StatusOK)
	case <-ctx.Done():
		core.WriterErrorResponse(w, "Not working", http.StatusInternalServerError)
		return
	}
}
