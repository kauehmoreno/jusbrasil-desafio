package handler

import (
	"context"
	"net/http"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/court"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"
)

//LegalProcessHandler represnets legal process handler
type LegalProcessHandler struct {
	db.DB
	cache.Cache
}

//NewLegalProcessHandler return a instance of handler
func NewLegalProcessHandler(db db.DB, cache cache.Cache) LegalProcessHandler {
	return LegalProcessHandler{
		db,
		cache,
	}
}

//GetByProcessID based on processID it will lokk for every stage of that court
func (lp LegalProcessHandler) GetByProcessID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	id := params.ByName("id")
	if id == "" {
		core.WriterErrorResponse(w, "legal process not found", http.StatusNotFound)
		return
	}

	manager, err := court.NewManager(court.Listeable(lp.Cache), court.Cacheable(lp.Cache), court.Database(lp.DB))
	if err != nil {
		log.Errorf("[handler.legalprocess.GetByProcessID] error on retrieve manager processID:%s - %v", id, err)
		core.WriterErrorResponse(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	ctx, cancel := context.WithTimeout(req.Context(), time.Millisecond*500)
	defer cancel()
	ch := make(chan struct {
		lp  []court.Process
		err error
	})
	go func() {
		lps, err := manager.ByProcessID(ctx, id)
		ch <- struct {
			lp  []court.Process
			err error
		}{lps, err}
		return
	}()

	select {
	case <-ctx.Done():
		log.Errorf("[handler.legalprocess.GetByProcessID] context timeout on retrieving processID %s", id)
		core.WriterErrorResponse(w, http.StatusText(http.StatusRequestTimeout), http.StatusRequestTimeout)
		return
	case result := <-ch:
		if result.err != nil {
			log.Errorf("[handler.legalprocess.GetByProcessID] error while retrieving processID %s - %v", id, result.err)
			core.WriterErrorResponse(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			return
		}
		core.WriteResponse(w, result.lp, http.StatusOK)
	}
}
