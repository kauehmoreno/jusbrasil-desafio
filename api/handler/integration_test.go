// +test integration

package handler_test

import (
	"fmt"
	"net/http"
	"testing"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/court"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/job"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"github.com/stretchr/testify/suite"
)

type IntegratationTest struct {
	processID, court, healthechkURL, jobRegisterURL, jobWatchURL, legalProcessURL string
	suite.Suite
}

func TestIntegrationSuite(t *testing.T) {
	suite.Run(t, new(IntegratationTest))
}

func (suite *IntegratationTest) SetupSuite() {
	host := settings.Get().Host
	suite.healthechkURL = fmt.Sprintf("%s/healthcheck", host)
	suite.processID = "0710802-55.2018.8.02.0001"
	suite.court = "tjal"
	suite.jobRegisterURL = fmt.Sprintf("%s/register/consumer", host)
	suite.jobWatchURL = fmt.Sprintf("%s/job/%s", host, suite.processID)
	suite.legalProcessURL = fmt.Sprintf("%s/court/process/%s", host, suite.processID)

	header := map[string]string{"Vary": "Accept-Encoding"}
	resp, err := core.Get(suite.healthechkURL, header)
	if err != nil {
		suite.T().Skipf("API is probably down - to run integration test please run `make run` and than `make integration-test`. Skiping tests %s...", suite.T().Name())
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		suite.T().Skipf("API is not responding well please check if there is any deep problem skiping tests %s...", suite.T().Name())
	}
}

func (suite IntegratationTest) TestIntegration() {
	suite.RegisterJob()
	time.AfterFunc(time.Second*3, suite.JobWatch)
	time.AfterFunc(time.Second*5, suite.LegalProcessWatch)
	suite.RegisterJobFailDueToExistedOne()
}

func (suite IntegratationTest) RegisterJob() {
	header := map[string]string{"Content-Type": "Application/json"}
	resp, err := core.Post(suite.jobRegisterURL, header, core.RegisterJob{ProcessID: suite.processID, Court: suite.court})
	suite.Require().NoError(err, "should not fail to register a non existing job")
	defer resp.Body.Close()
	suite.Require().Equal(resp.StatusCode, http.StatusAccepted, "status could shoud be 202")
	var rs core.JobResponse
	err = core.Decode(resp.Body, &rs)
	suite.Require().NoError(err, "should fail to decode body into expected response")
	suite.Require().Equal(suite.jobWatchURL, rs.JobWatch, "both url shoud be equal")
}

func (suite IntegratationTest) JobWatch() {
	header := map[string]string{"Content-Type": "Application/json"}
	resp, err := core.Get(suite.jobWatchURL, header)
	suite.Require().NoError(err, "should not fail to watch an existing job")
	defer resp.Body.Close()
	var jb job.Job
	err = core.Decode(resp.Body, jb)
	suite.Require().NoError(err, "should fail to decode body into expected response")

	suite.Require().Equal(suite.processID, jb.ProcessID, "processID should match on jobWatch")
	suite.Require().NotEmpty(jb.ID, "should contains id on jobWatch")
	suite.Require().Equal(suite.court, jb.Court, "court name should be the same on jobWatch")
	suite.Require().NotEmpty(jb.StartAt, "should contains start date on jobWatch")
	suite.Require().False(jb.Error.Error, "should not have error on jobWatch")
}

func (suite IntegratationTest) LegalProcessWatch() {
	header := map[string]string{"Content-Type": "Application/json"}

	resp, err := core.Get(suite.legalProcessURL, header)
	suite.Require().NoError(err, "should not fail to watch an existing legal process")
	defer resp.Body.Close()

	var lp []court.Process
	err = core.Decode(resp.Body, &lp)
	suite.Require().NoError(err, "should fail to decode body into expected response of legal process")
	suite.Require().Condition(func() bool {
		return len(lp) >= 1
	}, "should have at least one item")
	suite.Require().Equal(suite.processID, lp[0].LegalProcess.ProcessID, "on legal process consuption processID should matchup")
	suite.Require().Equal(suite.court, lp[0].Court, "on legal process consuption court should be equal to expected one")
}

func (suite IntegratationTest) RegisterJobFailDueToExistedOne() {
	header := map[string]string{"Content-Type": "Application/json"}
	resp, err := core.Post(suite.jobRegisterURL, header, core.RegisterJob{ProcessID: suite.processID, Court: suite.court})
	suite.Require().NoError(err, "should not fail to register a non existing job")
	defer resp.Body.Close()
	suite.Require().Equal(resp.StatusCode, http.StatusConflict, "status could shoud be 409")
	var rs core.BaseResponse

	err = core.Decode(resp.Body, &rs)
	suite.Require().NoError(err, "should fail to decode body into expected response")

	suite.Require().Equal("job already exist to this proccess ID", rs.Message, "message should match up")
	suite.Require().Equal(http.StatusConflict, rs.Status, "status should be 409")
}
