package handler

import (
	"context"
	"net/http"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/job"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"

	log "github.com/sirupsen/logrus"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"github.com/julienschmidt/httprouter"
)

//RegistrationHandler embeded manager to make perform operation easier
type RegistrationHandler struct {
	db.DB
	cache.Cacheable
}

//NewRegistrationHandler is used on router to access registration Handler
func NewRegistrationHandler(db db.DB, cache cache.Cacheable) RegistrationHandler {
	return RegistrationHandler{
		db,
		cache,
	}
}

//Register a crawler process base on a processID and court identification
func (r RegistrationHandler) Register(w http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	var body core.RegisterJob

	logger := log.Fields{}
	logger["handler"] = "RegistrationHandler"

	if err := core.Decode(req.Body, &body); err != nil {
		logger["error"] = err
		log.WithFields(logger).Error(" error on decode body")
		core.WriterErrorResponse(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}
	defer req.Body.Close()

	logger["process_id"] = body.ProcessID

	jb, _ := job.ByProcessID(req.Context(), body.ProcessID)(r.Cacheable, r.DB)
	if jb.ProcessID == body.ProcessID {
		core.WriterErrorResponse(w, "job already exist to this proccess ID", http.StatusConflict)
		return
	}

	q, err := queue.Connect()
	if err != nil {
		logger["error"] = err
		log.WithFields(logger).Error("error on connect with queue")
		core.WriterErrorResponse(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	defer q.Close()

	topic, err := queue.TopicBasedOnCourt(body.Court)
	if err != nil {
		core.WriterErrorResponse(w, "court not found", http.StatusNotFound)
		return
	}

	if err := q.Publish(topic, []byte(body.ProcessID)); err != nil {
		logger["error"] = err
		log.WithFields(logger).Error("error on publish into queue")
		core.WriterErrorResponse(w, "could no process information", http.StatusConflict)
		return
	}

	erro := job.Start(req.Context(), body.ProcessID, body.Court)(r.Cacheable, r.DB)
	if erro != nil {
		logger["error"] = erro
		log.WithFields(logger).Error("error on start job into DB")
		if err.Error() == context.DeadlineExceeded.Error() {
			core.WriteResponse(w, core.BaseResponse{
				Message: "timeout operation",
				Status:  http.StatusRequestTimeout,
			}, http.StatusRequestTimeout)
			return
		}
		core.WriterErrorResponse(w, "error on register job", http.StatusConflict)
		return
	}
	core.WriteResponse(w, core.NewJobWatch(body.ProcessID), http.StatusAccepted)
}

//RetrieveByProcessID based on processID it will return job status
func (r RegistrationHandler) RetrieveByProcessID(w http.ResponseWriter, req *http.Request, params httprouter.Params) {
	processID := params.ByName("processID")

	if processID == "" {
		core.WriterErrorResponse(w, "process not found", http.StatusNotFound)
		return
	}
	ctx, cancel := context.WithTimeout(req.Context(), time.Second)
	defer cancel()

	ch := make(chan struct {
		jb  job.Job
		err error
	})

	go func() {
		jb, err := job.ByProcessID(req.Context(), processID)(r.Cacheable, r.DB)
		ch <- struct {
			jb  job.Job
			err error
		}{jb, err}
		return
	}()

	select {
	case <-ctx.Done():
		core.WriterErrorResponse(w, "timeout operation", http.StatusRequestTimeout)
		return
	case result := <-ch:
		if result.err != nil && result.err != mongo.ErrNoDocuments {
			log.Errorf("[handler.job.RetrieveByProcessID] erro on retrieve job by processID %s - %v", processID, result.err)
			core.WriterErrorResponse(w, "something got wrong", http.StatusInternalServerError)
			return
		}
		core.WriteResponse(w, result.jb, http.StatusOK)
	}
}
