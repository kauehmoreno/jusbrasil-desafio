package job_test

import (
	"context"
	"errors"
	"testing"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/job"

	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
)

type mockCacheable struct {
	mock.Mock
}

func (m *mockCacheable) Get(s string, v interface{}) error {
	args := m.Called(s, v)
	return args.Error(0)
}

func (m *mockCacheable) Set(s string, v interface{}, t time.Duration) error {
	args := m.Called(s, v, t)
	return args.Error(0)
}
func (m *mockCacheable) Del(k ...string) error {
	args := m.Called(k)
	return args.Error(0)
}

type mockDB struct {
	mock.Mock
}

func (m *mockDB) Get(c context.Context, t string, q, e interface{}) error {
	args := m.Called(c, t, q, e)
	return args.Error(0)
}
func (m *mockDB) Filter(c context.Context, t string, q, e interface{}) error {
	args := m.Called(c, t, q, e)
	return args.Error(0)
}

func (m *mockDB) Insert(c context.Context, t string, i interface{}) error {
	args := m.Called(c, t, i)
	return args.Error(0)
}

func (m *mockDB) Update(c context.Context, t string, q, d interface{}) error {
	args := m.Called(c, t, q, d)
	return args.Error(0)
}

func (m *mockDB) Delete(c context.Context, t string, q interface{}) error {
	args := m.Called(c, t, q)
	return args.Error(0)
}

type testDBContextJobSuiteCase struct {
	suite.Suite
	*mockCacheable
	*mockDB
}

func TestDBContextJobCase(t *testing.T) {
	suite.Run(t, new(testDBContextJobSuiteCase))
}

func (suite *testDBContextJobSuiteCase) SetupSuite() {
	suite.mockCacheable = new(mockCacheable)
	suite.mockDB = new(mockDB)
}

func (suite testDBContextJobSuiteCase) TestStartJobWithoutErr() {
	jobID := "432423423423"
	key := cache.FormatKey(cache.JobProcessID, jobID)
	ctx := context.Background()

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Set", key, mock.Anything, cache.OneDay).Return(nil)
	dbMock.On("Insert", mock.Anything, settings.Get().DB.Read.Table.Job.Table, mock.Anything).Return(nil)

	err := job.Start(ctx, jobID, "tjfs")(cacheMock, dbMock)
	suite.Require().NoError(err, "should not return error on start a job")
}

func (suite testDBContextJobSuiteCase) TestStartJobShouldReturnErrWhenDBInsertFail() {
	jobID := "12112312312432423423423"
	key := cache.FormatKey(cache.JobID, jobID)
	ctx := context.Background()
	customErr := errors.New("my custom error")

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Set", key, mock.Anything, cache.OneDay).Return(nil)
	dbMock.On("Insert", mock.Anything, settings.Get().DB.Read.Table.Job.Table, mock.Anything).Return(customErr)

	err := job.Start(ctx, jobID, "tjfs")(cacheMock, dbMock)
	suite.Require().EqualError(err, customErr.Error(), "Both err should match")
}

func (suite testDBContextJobSuiteCase) TestStartJobShouldReturnErrOfContextExceedTimeout() {
	jobID := "432423423423"
	key := cache.FormatKey(cache.JobID, jobID)
	ctx := context.Background()
	customErr := errors.New("my custom error")
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Set", key, mock.Anything, cache.OneDay).Return(nil)
	dbMock.On("Insert", mock.Anything, settings.Get().DB.Read.Table.Job.Table, mock.Anything).Return(customErr).WaitUntil(time.After(time.Second * 3))

	err := job.Start(ctx, jobID, "tjfs")(cacheMock, dbMock)
	suite.Require().EqualError(err, context.DeadlineExceeded.Error(), "Both err should match")
}

func (suite testDBContextJobSuiteCase) TestCancelJobShouldFailWheneverDBOperationFail() {
	jobID := "432423423423"
	ctx := context.Background()
	customErr := errors.New("my custom error")
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	dbMock.On("Update", ctx, settings.Get().DB.Read.Table.Job.Table, mock.Anything, mock.Anything).Return(customErr)

	err := job.Cancel(ctx, jobID, customErr)(cacheMock, dbMock)
	suite.Require().Error(err, "should fail based on db failure")
}

func (suite testDBContextJobSuiteCase) TestSucessFullyOPeration() {
	jobID := "432423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	dbMock.On("Update", ctx, settings.Get().DB.Read.Table.Job.Table, mock.Anything, mock.Anything).Return(nil)
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)
	cacheMock.On("Get", key, mock.Anything).Return(nil)

	err := job.Cancel(ctx, jobID, errors.New("my error"))(cacheMock, dbMock)
	suite.Require().NoError(err, "should not return error from this operation")
}

func (suite testDBContextJobSuiteCase) TestShouldFailOnCancelatingADoneJob() {
	jobID := "432423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	dbMock.On("Update", ctx, settings.Get().DB.Read.Table.Job.Table, mock.Anything, mock.Anything).Return(nil)
	cacheMock.On("Get", key, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		jb := args.Get(1).(*job.Job)
		jb.Done()
	})
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)

	err := job.Cancel(ctx, jobID, errors.New("my error"))(cacheMock, dbMock)
	suite.Require().EqualError(err, "cannot change a finished job", "expected error does not match")
}

func (suite testDBContextJobSuiteCase) TestShouldReturnErrorWhenCancelIsCalledWithoutError() {
	jobID := "432423423423"
	ctx := context.Background()
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	err := job.Cancel(ctx, jobID, nil)(cacheMock, dbMock)
	suite.Require().EqualError(err, "an error must be provided to cancel")
}

func (suite testDBContextJobSuiteCase) TestDoneARunningJobShouldBeOk() {
	jobID := "432423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	dbMock.On("Update", ctx, settings.Get().DB.Read.Table.Job.Table, mock.Anything, mock.Anything).Return(nil)
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)
	cacheMock.On("Get", key, mock.Anything).Return(nil)

	err := job.Done(ctx, jobID)(cacheMock, dbMock)
	suite.Require().NoError(err, "should not return error from this operation")
}

func (suite testDBContextJobSuiteCase) TestDoneADoneJobShouldFail() {
	jobID := "3231212432423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	dbMock.On("Update", ctx, settings.Get().DB.Read.Table.Job.Table, mock.Anything, mock.Anything).Return(nil)
	cacheMock.On("Get", key, mock.Anything).Return(nil).Run(func(args mock.Arguments) {
		jb := args.Get(1).(*job.Job)
		jb.Done()
	})
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)
	err := job.Done(ctx, jobID)(cacheMock, dbMock)
	suite.Require().EqualError(err, "cannot change a finished job", "expected error does not match")
}

func (suite testDBContextJobSuiteCase) TestByIDShouldReturnFromCacheFirst() {
	jobID := "4324234234231212121212"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobID, jobID)

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Get", key, mock.Anything).Return(nil)
	dbMock.AssertNotCalled(suite.T(), "Get", ctx, mock.Anything, mock.Anything, mock.Anything)

	_, err := job.ByID(ctx, jobID)(cacheMock, dbMock)
	suite.Require().NoError(err, "should not fail on get a job by ID")
}

func (suite testDBContextJobSuiteCase) TestByIDAfterFailOnCacheForAnyReasonShouldExecDB() {
	jobID := "432423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobID, jobID)

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Get", key, mock.Anything).Return(errors.New("redis: Nil"))
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)

	dbMock.On("Get", ctx, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	job.ByID(ctx, jobID)(cacheMock, dbMock)
	dbMock.AssertNumberOfCalls(suite.T(), "Get", 1)
	dbMock.AssertCalled(suite.T(), "Get", ctx, mock.Anything, mock.Anything, mock.Anything)
}

func (suite testDBContextJobSuiteCase) TestByIDShouldNoSetOnCacheIFDBFails() {
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)
	jobID := "1243242342342323443"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobID, jobID)

	cacheMock.On("Get", key, mock.Anything).Return(errors.New("Nil"))

	dbMock.On("Get", ctx, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))

	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)
	job.ByID(ctx, jobID)(cacheMock, dbMock)

	cacheMock.AssertNumberOfCalls(suite.T(), "Set", 0)
	cacheMock.AssertNotCalled(suite.T(), "Set", key, mock.Anything, cache.OneWeek)
}

func (suite testDBContextJobSuiteCase) TestByProcessIDShouldReturnFromCacheFirst() {
	jobID := "4324234234231212"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Get", key, mock.Anything).Return(nil)
	dbMock.AssertNotCalled(suite.T(), "Filter", ctx, mock.Anything, mock.Anything, mock.Anything)

	_, err := job.ByProcessID(ctx, jobID)(cacheMock, dbMock)
	suite.Require().NoError(err, "should not fail on get a job by ID")
}

func (suite testDBContextJobSuiteCase) TestByProcessIDAfterFailOnCacheForAnyReasonShouldExecDB() {
	jobID := "421231232423423423"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)
	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Get", key, mock.Anything).Return(errors.New("redis: Nil"))
	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)

	dbMock.On("Get", ctx, mock.Anything, mock.Anything, mock.Anything).Return(nil)

	job.ByProcessID(ctx, jobID)(cacheMock, dbMock)
	dbMock.AssertNumberOfCalls(suite.T(), "Get", 1)
	dbMock.AssertCalled(suite.T(), "Get", ctx, mock.Anything, mock.Anything, mock.Anything)
}

func (suite testDBContextJobSuiteCase) TestByProcessIDShouldNoSetOnCacheIFDBFails() {
	jobID := "4322121231278686"
	ctx := context.Background()
	key := cache.FormatKey(cache.JobProcessID, jobID)

	cacheMock := new(mockCacheable)
	dbMock := new(mockDB)

	cacheMock.On("Get", key, mock.Anything).Return(errors.New("redis: Nil"))

	dbMock.On("Get", ctx, mock.Anything, mock.Anything, mock.Anything).Return(errors.New("error"))

	cacheMock.On("Set", key, mock.Anything, cache.OneWeek).Return(nil)
	job.ByProcessID(ctx, jobID)(cacheMock, dbMock)

	cacheMock.AssertNumberOfCalls(suite.T(), "Set", 0)
	cacheMock.AssertNotCalled(suite.T(), "Set", key, mock.Anything, cache.OneWeek)
}
