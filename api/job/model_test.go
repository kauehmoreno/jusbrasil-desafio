package job_test

import (
	"errors"
	"fmt"
	"testing"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"github.com/stretchr/testify/suite"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/job"
)

type testJobSuiteCase struct {
	suite.Suite
}

func TestJobCase(t *testing.T) {
	suite.Run(t, new(testJobSuiteCase))
}

func (suite testJobSuiteCase) TestJobCancelation() {
	jb := job.New("432492034820", "tjsp")

	suite.Require().Equal(jb.Status, job.Running)

	err := jb.Canceled()
	suite.Require().NoError(err, "should fail to cancel a running job")
	suite.Require().Equal(jb.Status, job.Canceled)
}

func (suite testJobSuiteCase) TestJobDone() {
	jb := job.New("432492034820", "tjsp")

	suite.Require().Equal(jb.Status, job.Running)

	err := jb.Done()
	suite.Require().NoError(err, "should fail to done a running job")
	suite.Require().Equal(jb.Status, job.Finished)
}

func (suite testJobSuiteCase) TestCancelatingADoneJobShouldFail() {
	jb := job.New("432492034820", "tjsp")

	suite.Require().Equal(jb.Status, job.Running)

	err := jb.Done()
	suite.Require().NoError(err, "should fail to done a running job")
	suite.Require().Equal(jb.Status, job.Finished)

	err = jb.Canceled()
	suite.Require().EqualError(err, "cannot change a finished job")
}

func (suite testJobSuiteCase) TestDoneJobCannotBeRunningAgain() {
	jb := job.New("432492034820", "tjsp")

	suite.Require().Equal(jb.Status, job.Running)

	err := jb.Done()
	suite.Require().NoError(err, "should fail to done a running job")
	suite.Require().Equal(jb.Status, job.Finished)

	err = jb.Running()
	suite.Require().EqualError(err, "cannot change a finished job")
}

func (suite testJobSuiteCase) TestAppendErrorToAJob() {
	jb := job.New("432492034820", "tjsp")
	customErr := errors.New("my custom error")
	jb.AppendError(customErr)
	suite.Require().True(jb.Error.Error)
	suite.Require().Equal(customErr.Error(), jb.Error.Msg, "both msg should match")
}

func (suite testJobSuiteCase) TestNewShouldContainsAIDOnEveryInstance() {
	jb := job.New("432492034820", "tjsp")
	suite.Require().NotEmpty(jb.ID, "should contains ID")
	jb2 := job.New("4324920348221210", "tjsm")
	suite.Require().NotEmpty(jb2.ID, "should contains ID")
	suite.Require().NotEqual(jb.ID, jb2.ID, "should be differents from each other")
}

func (suite testJobSuiteCase) TestNewShouldInitializeWithNoError() {
	jb := job.New("432492034820", "tjsp")
	suite.Require().False(jb.Error.Error, "No error should accour")
}

func (suite testJobSuiteCase) TestNewShouldInitializeWithStartAt() {
	jb := job.New("432492034820", "tjsp")
	suite.Require().NotEmpty(jb.StartAt, "should initialize with a starting date")
}

func (suite testJobSuiteCase) TestIncludeLegalProcessURLConsuption() {
	jb := job.New("1012", "tjal")

	err := jb.Done()
	suite.Require().NoError(err, "shouldnt fail to finish job")

	err = jb.IncludeLegalProcessURL()
	suite.Require().NoError(err, "shouldnt fail to include legal process url on finished job")
	expected := fmt.Sprintf("%s/court/process/%s", settings.Get().Host, jb.ProcessID)
	suite.Require().Equal(expected, jb.LegalProcessURL, "result does not match with expected one")
}

func (suite testJobSuiteCase) TestIncludeLegalProcessURLConsuptionShouldFailWhenJobIsNotFinished() {
	jb := job.New("1012", "tjal")
	err := jb.IncludeLegalProcessURL()
	suite.Require().Error(err, "should fail to include url from legal process of a running job")
}

func (suite testJobSuiteCase) TestIncludeLegalProcessURLConsuptionShouldFailWhenJobCanceled() {
	jb := job.New("1012", "tjal")
	err := jb.Canceled()
	suite.Require().NoError(err, "shouldnt fail to finish job")

	err = jb.IncludeLegalProcessURL()
	suite.Require().Error(err, "should fail to include url from legal process of a CANCELED job")
}
