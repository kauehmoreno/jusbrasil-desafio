package job

import (
	"context"
	"errors"
	"fmt"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"

	"go.mongodb.org/mongo-driver/bson"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"
)

// Task interface to be implemented to every job with fullfill
type Task interface {
	Start(ctx context.Context, processID, court string) error
	Cancel(ctx context.Context, id string) error
	Done(ctx context.Context, id string) error
}

type dbRepo func(database db.Writer) error

func create(ctx context.Context, job Job) dbRepo {
	return func(database db.Writer) error {
		return database.Insert(ctx, settings.Get().DB.Write.Table.Job.Table, job)
	}
}

func change(ctx context.Context, id, status string) dbRepo {
	legalProcessURL := fmt.Sprintf("%s/court/process/%s", settings.Get().Host, id)
	return func(database db.Writer) error {
		return database.Update(
			ctx,
			settings.Get().DB.Write.Table.Job.Table,
			bson.D{
				{"process_id", id},
			},
			bson.D{
				{"$set", bson.D{
					{"status", status},
					{"ended_at", core.GetCurrTimeStamp()},
					{"legal_process_url", legalProcessURL},
				}},
			},
		)
	}
}

func appendError(ctx context.Context, id string, err error) dbRepo {
	return func(database db.Writer) error {
		return database.Update(
			ctx,
			settings.Get().DB.Write.Table.Job.Table,
			bson.D{
				{"process_id", id},
			},
			bson.D{
				{"$set", bson.D{
					{"status", Canceled},
					{"error.has_error", true},
					{"error.message", err.Error()},
					{"ended_at", core.GetCurrTimeStamp()},
				}},
			},
		)
	}
}

type Repository struct {
	WriterRepository
	ReaderRepository
}

//WriterRepository has DB interface
type WriterRepository func(cache.Cacheable, db.DB) error

//ReaderRepository has db READER only and always return a job
type ReaderRepository func(cache.Cacheable, db.Reader) (Job, error)

//Start register a job starting on DB and on Cache
func Start(pCtx context.Context, processID, court string) WriterRepository {
	return func(storage cache.Cacheable, database db.DB) error {
		ctx, cancel := context.WithTimeout(pCtx, time.Second)
		defer cancel()

		job := New(processID, court)
		ch := make(chan struct{ err error })

		go func(ctx context.Context) {
			err := create(ctx, job)(database)
			ch <- struct{ err error }{err}
			return
		}(ctx)

		select {
		case <-ctx.Done():
			return ctx.Err()
		case creation := <-ch:
			if creation.err != nil {
				return creation.err
			}
			key := cache.FormatKey(cache.JobProcessID, processID)
			defer storage.Set(key, job, cache.OneDay)
			return nil
		}
	}
}

//Cancel change job status to canceled on DB and on cache
func Cancel(ctx context.Context, id string, err error) WriterRepository {
	return func(storage cache.Cacheable, database db.DB) error {
		if err == nil {
			return errors.New("an error must be provided to cancel")
		}

		if err := appendError(ctx, id, err)(database); err != nil {
			return err
		}

		job, err := ByProcessID(ctx, id)(storage, database)
		if err != nil {
			return err
		}

		if err := job.Canceled(); err != nil {
			return err
		}

		job.AppendError(err)
		key := cache.FormatKey(cache.JobProcessID, id)
		defer storage.Set(key, job, cache.OneWeek)
		return nil
	}
}

//Done finish a job status to Done on DB and on Cache
func Done(ctx context.Context, id string) WriterRepository {
	return func(storage cache.Cacheable, database db.DB) error {
		job, err := ByProcessID(ctx, id)(storage, database)
		if err != nil {
			return err
		}

		if err := job.Done(); err != nil {
			return err
		}

		if err := job.IncludeLegalProcessURL(); err != nil {
			return err
		}

		if err := change(ctx, id, Finished)(database); err != nil {
			return err
		}

		key := cache.FormatKey(cache.JobProcessID, id)
		defer storage.Set(key, job, cache.OneWeek)
		return nil
	}
}

//ByID retrieve a job byID
func ByID(ctx context.Context, id string) ReaderRepository {
	return func(storage cache.Cacheable, database db.Reader) (Job, error) {
		var job Job
		key := cache.FormatKey(cache.JobID, id)
		if err := storage.Get(key, &job); err != nil {
			erro := database.Get(
				ctx,
				settings.Get().DB.Read.Table.Job.Table,
				bson.D{{"id", id}},
				&job,
			)
			if erro != nil {
				return job, erro
			}
			defer storage.Set(key, job, cache.OneWeek)
			return job, nil
		}
		return job, nil
	}
}

//ByProcessID retrieve a job by processID
func ByProcessID(ctx context.Context, id string) ReaderRepository {
	return func(storage cache.Cacheable, database db.Reader) (Job, error) {
		var job Job
		key := cache.FormatKey(cache.JobProcessID, id)
		if err := storage.Get(key, &job); err != nil {
			erro := database.Get(
				ctx,
				settings.Get().DB.Read.Table.Job.Table,
				bson.D{{"process_id", id}},
				&job,
			)
			if erro != nil {
				return job, erro
			}
			defer storage.Set(key, job, cache.OneWeek)
			return job, nil
		}
		return job, nil
	}
}
