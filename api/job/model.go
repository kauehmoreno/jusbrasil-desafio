package job

import (
	"errors"
	"fmt"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/core"
)

const (
	// Finished define when is process is done
	Finished = "F"
	// Running is the current state of running
	Running = "R"
	// Canceled is a job canceled
	Canceled = "C"
)

//Job represent a job to be watched of every income action
type Job struct {
	ID              string  `json:"id" bson:"id"`
	Status          string  `json:"status" bson:"status"`
	StartAt         string  `json:"started_at" bson:"started_at"`
	EndAt           string  `json:"ended_at" bson:"ended_at"`
	Court           string  `json:"court" bson:"court"`
	ProcessID       string  `json:"process_id" bson:"process_id"`
	LegalProcessURL string  `json:"legal_process_url" bson:"legal_process_url"`
	Error           jobErro `json:"error" bson:"error"`
}

type jobErro struct {
	Error bool   `json:"has_error" bson:"has_error"`
	Msg   string `json:"message" bson:"message"`
}

//New return a new job
func New(processID, court string) Job {
	return Job{
		ID:        core.GenerateUUID(),
		Status:    Running,
		ProcessID: processID,
		Court:     court,
		StartAt:   core.GetCurrTimeStamp(),
		EndAt:     "",
		Error: jobErro{
			Error: false,
			Msg:   "",
		},
	}
}

// Done change job status to done
func (j *Job) Done() error {
	return j.status(Finished)
}

//IncludeLegalProcessURL include acessing url when is done
func (j *Job) IncludeLegalProcessURL() error {
	if j.Status != Finished {
		return errors.New("info must be available only on finished job")
	}
	j.LegalProcessURL = fmt.Sprintf("%s/court/process/%s", settings.Get().Host, j.ProcessID)
	return nil
}

// Canceled change job status to canceled
func (j *Job) Canceled() error {
	return j.status(Canceled)
}

// Running change job status to running
func (j *Job) Running() error {
	return j.status(Running)
}

func (j *Job) status(status string) error {
	if j.Status == Finished {
		return errors.New("cannot change a finished job")
	}
	j.EndAt = core.GetCurrTimeStamp()
	j.Status = status
	return nil
}

// AppendError whenever there is a error it allow to append into job info
func (j *Job) AppendError(err error) {
	j.Error.Error = true
	j.Error.Msg = err.Error()
}
