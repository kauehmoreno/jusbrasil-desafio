package main

import (
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/consumer"
)

func init() {
	log.SetFormatter(&log.TextFormatter{DisableSorting: true})
	log.SetLevel(log.WarnLevel)
}

func main() {

	cm := consumer.New(consumer.RetryTime(time.Minute), consumer.WithTicker(time.Second*10))
	config := settings.Get().Crawler.TJAL
	log.Warn(config)
	worker := consumer.NewTJSP(
		cm, consumer.NumOfCrawlers(5),
		consumer.URLVisitors(config.URLS...),
		consumer.RateLimit(10, time.Second*30),
	)

	if err := worker.Start(); err != nil {
		log.Fatalf("[worker.crawler.tjal] could not start worker %v", err)
	}
	log.Warn("[worker.crawler.tjal] has started sucessfully")
	select {}
}
