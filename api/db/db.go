package db

import (
	"context"
	"encoding/json"
	"errors"
	"sync"
	"time"

	log "github.com/sirupsen/logrus"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"

	"go.mongodb.org/mongo-driver/mongo"
)

var (
	db   *Database
	once sync.Once
	pool = sync.Pool{
		New: func() interface{} {
			return getInstance()
		},
	}
)

// Reader expose reading operation on database
type Reader interface {
	Get(ctx context.Context, table string, query, expected interface{}) error
	Filter(ctx context.Context, table string, query, expected interface{}) error
}

// Writer expose only write operation on database
type Writer interface {
	Insert(ctx context.Context, table string, item interface{}) error
	Update(ctx context.Context, table string, query, data interface{}) error
	Delete(ctx context.Context, table string, query interface{}) error
}

// DB is a composible interface of both reader and writer
type DB interface {
	Reader
	Writer
}

//Database struct repre of a database
type Database struct {
	timeoutOp time.Duration
	*mongo.Database
}

func getInstance(cliOpts ...Options) *Database {
	once.Do(func() {
		ctx, cancel := context.WithTimeout(context.Background(), settings.Get().DB.Read.ConnTimeout)
		defer cancel()

		opts := options.Client()
		opts.SetMaxPoolSize(settings.Get().DB.Read.MaxPoolSize)
		opts.SetAppName("justbrasil")
		opts.SetMaxConnIdleTime(settings.Get().DB.Write.MaxIdle)
		opts.ApplyURI(settings.Get().DB.Read.User)
		cli, erro := mongo.Connect(ctx, opts)
		if erro != nil {
			log.Errorf("[DB] error on connect to db %v", erro)
			return
		}
		database := &Database{0, cli.Database(settings.Get().DB.Read.Name)}
		for _, opts := range cliOpts {
			opts(database)
		}
		db = database
	})
	return db
}

// Options allow client to customize operation
type Options func(*Database)

// Connect use sync pool to retrieve reference from db instance
func Connect(opts ...Options) (*Database, error) {
	db := pool.Get().(*Database)
	if db != nil {
		defer pool.Put(db)
		return db, nil
	}
	if db = getInstance(opts...); db == nil {
		return nil, errors.New("erro on connect to db")
	}
	defer pool.Put(db)
	return db, nil
}

func (db Database) Get(ctx context.Context, table string, query, expected interface{}) error {
	collection := db.Collection(table)
	return collection.FindOne(ctx, query).Decode(&expected)
}

func (db Database) Filter(ctx context.Context, table string, query, expected interface{}) error {
	limit := int64(20)
	c, err := db.Collection(table).Find(ctx, query, &options.FindOptions{Limit: &limit})
	if err != nil {
		return err
	}
	defer c.Close(ctx)
	var result []bson.M
	for c.Next(ctx) {
		var el bson.M
		if err := c.Decode(&el); err != nil {
			return err
		}
		result = append(result, el)
	}
	data, err := json.Marshal(result)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, &expected)
}

func (db Database) Insert(ctx context.Context, table string, item interface{}) error {
	_, err := db.Collection(table).InsertOne(ctx, item)
	return err
}

func (db Database) Update(ctx context.Context, table string, query, data interface{}) error {
	_, err := db.Collection(table).UpdateOne(ctx, query, data)
	return err
}
func (db Database) Delete(ctx context.Context, table string, query interface{}) error {
	_, err := db.Collection(table).DeleteOne(ctx, query)
	return err
}
