package queue

import (
	"errors"
	"os"
	"strings"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"github.com/vmihailenco/msgpack"

	"github.com/sirupsen/logrus"

	nats "github.com/nats-io/go-nats"
)

var (
	user, pass, host string
)

// DataWithErr representation of data with error type
type DataWithErr struct {
	ProcessID string
	Err       error
}

// ErrorMsg allow send to error topic a message to with error and processID which has fail
func ErrorMsg(ProcessID string, err error) ([]byte, error) {
	msg := DataWithErr{
		ProcessID: ProcessID,
		Err:       err,
	}
	return msgpack.Marshal(msg)
}

const (
	// TopicLegalProcessTJSP represent a topic name available for useage in this case a topic about legal process
	// of TJSP
	TopicLegalProcessTJSP = "legal_process_topic_TJSP"
	// TopicLegalProcessTJMS represent a topic name of legal process of TJMS
	TopicLegalProcessTJMS = "legal_process_topic_TJMS"
	// topicLegalProcessWithErr represent a topic with will hold every not process data
	topicLegalProcessWithErr = "legal_process_topic_with_err"
)

func init() {
	user = os.Getenv("NATS_USER")
	pass = os.Getenv("NATS_PASS")
	host = os.Getenv("NATS_URL")
}

func conn() *nats.Conn {
	if host == "" {
		host = settings.Get().QueueHost
		if host == "" {
			host = nats.DefaultURL
		}
	}
	conn, err := nats.Connect(host, nats.UserInfo(user, pass))
	if err != nil {
		logrus.Errorf("[queue] couldnt connect with provider: %v - host %s", err, host)
		return nil
	}
	return conn
}

// Connect ... return a little simplier connection to nats
func Connect() (*nats.Conn, error) {
	nats := conn()
	if nats == nil {
		return nil, errors.New("nats - not connected")
	}
	return nats, nil
}

//TopicBasedOnCourt extract topic based on court name
func TopicBasedOnCourt(court string) (string, error) {
	switch strings.ToLower(court) {
	case "tjal":
		return TopicLegalProcessTJSP, nil
	case "tjms":
		return TopicLegalProcessTJMS, nil
	default:
		return "", errors.New("no topic for this court")
	}
}

// TopicWithErr use to compose topicName with error
func TopicWithErr(topicName string) string {
	var s strings.Builder
	s.WriteString(topicName)
	s.WriteString(":")
	s.WriteString(topicLegalProcessWithErr)
	return s.String()
}
