package cache

import "time"

const (
	// JobID cache key to represent every job by ID
	JobID = "Job_Key_ID"
	// JobProcessID cache key to represent every job by processID
	JobProcessID = "Job_Key_Process_ID"
	// LegalProcessID cache key to rep every legal process by ID
	LegalProcessID = "Legal_Process_ID"
	// LegalProcessByCourt cache key to rep process by court
	LegalProcessByCourt = "Legal_Process_By_Court"
)

// Expiration time

const (
	// OneHour expiration time
	OneHour = time.Hour
	//TwoHour expiration time
	TwoHour = time.Hour * 2
	//OneWeek expiration time
	OneWeek = time.Hour * 168
	//TeenMinutes expiration time
	TeenMinutes = time.Minute * 10
	//OneMinute expiration time
	OneMinute = time.Minute
	//OneMonth expiration time
	OneMonth = time.Hour * 672
	//OneDay expiration time
	OneDay = time.Hour * 24
	//NoExpiration time
	NoExpiration = 0
)
