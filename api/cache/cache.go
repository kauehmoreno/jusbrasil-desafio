package cache

import (
	"sync"
	"time"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"

	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
)

var (
	once    sync.Once
	storage *Storage
	pool    = sync.Pool{
		New: func() interface{} {
			return singleton()
		},
	}
)

// Storage represent a cache concrete type responsable to
// logicly put value available on cache
type Storage struct {
	l2 *redis.Client
}

// New returns storage reference to client
// it use sync.pool to low backpressure on GC as well as
// return a singleton instance
func New() *Storage {
	storage := pool.Get().(*Storage)
	defer pool.Put(storage)
	return storage
}

func options() *redis.Options {
	setting := settings.Get().Cache
	return &redis.Options{
		Addr:               setting.Addr,
		DB:                 setting.Db,
		Password:           setting.Pswd,
		MaxRetries:         setting.MaxRetries,
		MinRetryBackoff:    setting.MinRetryBackoff,
		MaxRetryBackoff:    setting.MaxRetryBackoff,
		DialTimeout:        setting.DialTimeout,
		ReadTimeout:        setting.ReadTimeout,
		WriteTimeout:       setting.WriteTimeout,
		PoolSize:           setting.PoolsizePerCluster,
		MinIdleConns:       setting.MinIdleConns,
		MaxConnAge:         setting.MaxConnAge,
		PoolTimeout:        setting.PoolTimeout,
		IdleTimeout:        setting.IddleTimeout,
		IdleCheckFrequency: setting.IdleCheckFrequency,
	}
}

func singleton() *Storage {
	once.Do(func() {
		storage = &Storage{
			l2: redis.NewClient(options()),
		}
	})
	return storage
}

// Get use msgpack to unmarshal value into cache and convert it to
// type expected
func (storage Storage) Get(k string, expected interface{}) error {
	k = hashKey(k)
	data, err := storage.l2.Get(k).Bytes()
	if err != nil {
		return err
	}
	return msgpack.Unmarshal(data, &expected)
}

// Set based on key it will packet value with msgpack and include exp on it
func (storage Storage) Set(k string, value interface{}, exp time.Duration) error {
	k = hashKey(k)
	data, err := msgpack.Marshal(value)
	if err != nil {
		return err
	}
	return storage.l2.Set(k, data, exp).Err()
}

// Del will delete all match keys from cache
// it allow multiple params
func (storage Storage) Del(keys ...string) error {
	hashKeys := []string{}
	for _, key := range keys {
		hashKeys = append(hashKeys, hashKey(key))
	}

	return storage.l2.Del(hashKeys...).Err()
}

// Ping used to healthcheck storage
func (storage Storage) Ping() error {
	return storage.l2.Ping().Err()
}

func (storage Storage) Push(key string, value interface{}) error {
	key = hashKey(key)
	return storage.l2.LPush(key, value).Err()
}

func (storage Storage) Range(key string, from, to int64) ([]string, error) {
	key = hashKey(key)
	return storage.l2.LRange(key, from, to).Result()
}

func (storage Storage) Rem(key string, value interface{}) error {
	key = hashKey(key)
	return storage.l2.LRem(key, 1, value).Err()
}

func (storage Storage) Len(k string) (int64, error) {
	k = hashKey(k)
	return storage.l2.LLen(k).Result()
}
