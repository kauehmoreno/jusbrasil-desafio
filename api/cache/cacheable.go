package cache

import (
	"strconv"
	"strings"
	"time"

	"github.com/cespare/xxhash"
)

// Cacheable interface is the minimum  cache representation on system
type Cacheable interface {
	Get(string, interface{}) error
	Set(string, interface{}, time.Duration) error
	Del(k ...string) error
}

type Listeable interface {
	Push(string, interface{}) error
	Range(key string, from, to int64) ([]string, error)
	Rem(string, interface{}) error
	Len(string) (int64, error)
}

type Cache interface{
	Cacheable
	Listeable
}

// HealthCache interface responsable to define contract of healthcheck Cache action
type HealthCache interface {
	Ping() error
}

func hashKey(k string) string {
	return strconv.FormatUint(xxhash.Sum64String(k), 10)
}

// FormatKey combine all params to a pattern as %s:%s:%s
// and its use to cache formatKey parttern
func FormatKey(name string, params ...string) string {
	var s strings.Builder
	s.WriteString(name)
	for _, param := range params {
		s.WriteString(":")
		s.WriteString(param)
	}
	return s.String()
}
