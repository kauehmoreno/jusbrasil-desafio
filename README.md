# Desafio Jusbrasil

[![JusBrasil](http://jggadvogados.com.br/wp-content/uploads/2018/09/jusbrasil.jpg)](https://www.jusbrasil.com.br/home)

# O que foi feito ? 
    - Api que registra um job basedo em um processID e um court name 
    - Registro do job num broker (Nats.io)
    - Workers que são crawlers que consomem das filas dos brokers
    - Processamento e crawling do recurso em background de maneira assíncrona
    - Job status (R(Running) F(Finished) C(Canceled))
    - Rota para consumo do dado assim que pronto com a instâncias especificadas
    - A instância são denominadas stage e são inteiros que representam primeira, segunda e etc...

### Tecnologia usada

* [Golang] - Usado para o backend e workers
* [Redis] - Usado para abstrair o conceito de cache
* [Nats] - Usado como broker para enfileirar os processos
* [mongo] - Usado para persistir os dados
* [Docker] - Usado para facilitar a configuração do ambiente local

### Comandos

Para executar o projeto é preciso executar os seguintes comandos:

```sh
$ make setup
$ make run 
$ ou se preferir make run-watch
```

Para excecução dos testes: 
```sh
$ make test
$ make integration-test

Problemas:
 - Caso hajá algum problema para executar o projeto lembre-se de calocar o projeto no $GOPATH para as execuções do Docker rodarem de maneira correta
```

### Explicação do desenvolvimento

Todo registro de um job é assíncrono, o mesmo irá registrar um Job e enfileirar o processo para crawlear aquele processo. Ao conectar com o nats o mesmo irá inserir esse job na fila.

Os workers rodam separados da aplicação declarados no docker-compose como outro serviço, todavia, eles estão num folder dentro do projeto para ter o beneficio de usar todos os packages de integração com os serviços(db, cache, queue). 

Foi criado um conceito de consumer e crawler para ser composto por diversos tipos, um padrão para facilitar o crawling de outras rotas em outros sites. Uma maneira de padronizar o desenvolvimento e tentar ganhar em escalabilidade e manutenabilidade. É possível customizar quantos workers serão instanciados por cada crawler/ ratelimit/ conconrrência de request

Ao criar um job é retornado a url de consumo para o job ser acompanhado ao cliente, nesse momento, o mesmo tem o status de R(Running).

Quando o crawler terminar de executar o processo ele irá alterar o status do JOB para D(Done) caso tudo esteja Ok ou C(Canceled) se algum erro acontecer. Todos os crawlers tem mecanismo de backoff e após as tentativas eles reinfileram os jobs na fila.

Em caso de erro os workers irão apendar a causa do erro no job para que seja possível consultar o mesmo na rota de job watch.

Quando o resultado do job é alterado para Done, o mesmo irá inserir a URL de consumo para aquele processo e será possível acessar a rota com todo o detalhamento daquele processo. Uma lista é retornada com todas as instancia encontradas.

### TODO:
 - Testes de benchmark para controlar o consumo de memória e cpu de alguns packages
 - Weebhook para notificar o client que um job terminou enviando a url para consumo do mesmo
 - Mais testes de integração 
 - Explorar mais os casos de erro
 

### ROTAS DA API 

Criação de um job:
```python
POST http://localhost/register/consumer/
Body:
{
	"process_id":"0710802-55.2018.8.02.0001",
	"court":"tjal"
}

Response:  status 202
{
    "job_watch": "http://localhost/job/0710802-55.2018.8.02.0001"
}

status != 200
{
    "message":"error blbalba"
	"status": "500 - 408 - 404 - 400 - 409"
}

```

Detalhes de um job:
```python
GET http://localhost/job/:processID

response 200 - se estiver rodando
{
    "id": "a3d25414-b26c-4c14-ac66-3eb4a156da0d",
    "status": "R",
    "started_at": "2019-07-02 14:52:33",
    "ended_at": "",
    "court": "tjal",
    "process_id": "0710802-55.2018.8.02.0001",
    "legal_process_url": "",
    "error": {
        "has_error": false,
        "message": ""
    }
}

response 200 - se estiver finalizado
{
    "id": "a3d25414-b26c-4c14-ac66-3eb4a156da0d",
    "status": "F",
    "started_at": "2019-07-02 14:52:33",
    "ended_at": "2019-07-02 14:54:33",
    "court": "tjal",
    "process_id": "0710802-55.2018.8.02.0001",
    "legal_process_url": "http://localhost/court/process/0710802-55.2018.8.02.0001",
    "error": {
        "has_error": false,
        "message": ""
    }
}

response 200 - se estiver cancelado
{
    "id": "a3d25414-b26c-4c14-ac66-3eb4a156da0d",
    "status": "C",
    "started_at": "2019-07-02 14:52:33",
    "ended_at": "2019-07-02 14:54:33",
    "court": "tjal",
    "process_id": "0710802-55.2018.8.02.0001",
    "legal_process_url": "",
    "error": {
        "has_error": true,
        "message": "error blabalbalbalba"
    }
}

status != 200
{
    "message":"error blbalba"
	"status": "500"
}
```

Detalhe de um processo
```python
GET http://localhost/court/process/:processID

status 500 
{
    "message": "Something bad happens please try again later",
    "status": 500
}

```
Healtcheck :
```python
GET http://localhost/healthcheck
Response:
status 200
WORKING! 

status 500
NOT WORKING! 

GET http://localhost/healthcheck/cache
Response:
status 200
{
    "message": "working",
    "status": 200
}

status 500
{
    "message": "Not working",
    "status": 500
}

GET http://localhost/healthcheck/queue
Response:
status 200
{
    "message": "working",
    "status": 200
}

status 500
{
    "message": "Not working",
    "status": 500
}

GET http://localhost/healthcheck/db
Response:
status 200
{
    "message": "working",
    "status": 200
}

status 500
{
    "message": "Not working",
    "status": 500
}
```

#### Testes
Há um json importado do postman para ser usado para facilitar os testes.

### Disclaimer 

O redis cluster não ficou com a configuração que eu queria, deu erro para buildar no docker compose devido aos hosts dinamicos - os slaves não estavam no sync com o master - toda as operações estavam dando erro menos o ping
Com isso modifiquei para o redis normal.

Gostaria de tentar pegar mais casos de possiveis bug


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)


   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
