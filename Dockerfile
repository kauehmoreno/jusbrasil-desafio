FROM golang:alpine As builder

RUN adduser -h $GOPATH -D -g '' jusbrasil-desafio
USER jusbrasil-desafio

WORKDIR $GOPATH/src/gitlab.com/kauehmoreno/jusbrasil-desafio
ADD --chown=jusbrasil-desafio . . 

RUN go build -o /tmp/jusbrasil-desafio main.go


FROM alpine:latest

RUN apk add -U tzdata \
    && cp /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime \
    && echo "America/Sao_Paulo" > /etc/timezone \
    && apk del tzdata

RUN adduser -h /api -D -g '' jusbrasil-desafio
USER jusbrasil-desafio

WORKDIR /api

COPY --chown=jusbrasil-desafio --from=builder /tmp/jusbrasil-desafio /api/jusbrasil-desafio
COPY --chown=jusbrasil-desafio --from=builder /go/src/gitlab.com/kauehmoreno/jusbrasil-desafio/deploy/settings_local.json /api/deploy/settings_local.json

EXPOSE 9000

ENTRYPOINT /api/jusbrasil-desafio