package main

import (
	"net/http"
	"syscall"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/queue"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/cache"

	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/db"

	"github.com/phyber/negroni-gzip/gzip"
	"github.com/rs/cors"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/negroni"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/router"
	"gitlab.com/kauehmoreno/jusbrasil-desafio/api/settings"
)

func init() {
	log.SetFormatter(&log.TextFormatter{DisableSorting: true})
	log.SetLevel(log.WarnLevel)
}

func main() {
	maxULimit()

	database, err := db.Connect()
	if err != nil {
		log.Fatalf("[api] could not connect to database %v", err)
	}

	cache := cache.New()
	if err := cache.Ping(); err != nil {
		log.Fatalf("[api] error on ping redis - %v", err)
	}

	queue, err := queue.Connect()
	if err != nil {
		log.Fatalf("[api] error on connect into queue - %v", err)
	}
	defer queue.Close()
	if !queue.IsConnected() {
		log.Fatalf("[api] queue is not connected %v", queue.Status())
	}

	r := router.New(database)

	ng := negroni.New(negroni.NewRecovery(), Cors())
	ng.Use(gzip.Gzip(gzip.BestSpeed))
	ng.UseHandler(r)

	server := http.Server{
		ReadTimeout:  settings.Get().ReadTimeout,
		WriteTimeout: settings.Get().WriteTimeout,
		IdleTimeout:  settings.Get().IdleTimeout,
		Handler:      ng,
		Addr:         settings.Get().Service,
	}

	log.Warn("[API] listen and serve on port 8888")
	server.ListenAndServe()

}

// Cors stabilish rules to access API endpoints
func Cors() *cors.Cors {
	return cors.New(cors.Options{
		AllowedOrigins: settings.Get().Cors.Allowed,
		AllowedMethods: settings.Get().Cors.AllowedMethods,
		AllowedHeaders: settings.Get().Cors.AllowedHeaders,
	})
}

// Increase resources limitations
func maxULimit() error {
	var uLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &uLimit); err != nil {
		log.Errorf("[maxUlimit] couldnt get ulimit on system %v", err)
		return err
	}
	log.Warnf("Current Ulimit %v", uLimit)
	uLimit.Cur = uLimit.Max
	log.Warnf("Changed to: %v", uLimit)
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &uLimit); err != nil {
		log.Errorf("[maxUlimit] couldnt set ulimit on system %v", err)
		return err
	}
	return nil
}
