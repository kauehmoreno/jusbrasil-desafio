test:
	@go test -v -count=1 --race ./api/...

integration-test:
	@go test -v -count=1 --race ./api/handler/...

run-nats:
	@nats-server -p 4222 -cluster nats://localhost:4248 -D

setup: build-docker

run:
	@docker-compose up -d

run-watch:
	@docker-compose up

down:
	@docker-compose down

full-down:
	@docker-compose down -v --remove-orphans

build-docker:
	@docker-compose build
